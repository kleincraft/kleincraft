#include "texture.h"

#include <png.h>

static const int MaxTextureSize = 512;

// Convert 32 bit texture to 16 bit texture...
static uint16_t* convertTexture( const uint8_t* src, const unsigned w, const unsigned h, const unsigned channels )
{
  uint16_t* dst = new uint16_t[ w * h ];
  
  unsigned j = 0;
  for( unsigned i = 0; i < w * h; ++i ) {
    
    if( channels == 3 ) {
      
      // RGB_5_6_5
      dst[ i ] = 
	( ( src[ j     ] >> 3 ) << 11 ) |
	( ( src[ j + 1 ] >> 2 ) <<  5 ) |
	( ( src[ j + 2 ] >> 3 ) );
    }
    else {
      
      // RGB_5_5_5_1
      dst[ i ] = 
	( ( src[ j     ] >> 3 ) << 11 ) |
	( ( src[ j + 1 ] >> 3 ) <<  6 ) |
	( ( src[ j + 2 ] >> 3 ) <<  1 ) |
	( ( src[ j + 3 ] != 0 ) ? 1 : 0 );
    }
    
    j += channels;
  }
  
  return dst;
}

Texture* Texture::load( const std::string& filename ) 
{ 
  FILE* fp = fopen( filename.c_str(), "rb" );
  if( !fp ) {
    
    perror( "fopen" );
    return 0;
  }
  
  const size_t SignatureSize = 8;
  png_byte signature[ SignatureSize ];
  
  size_t n = fread( signature, 1, SignatureSize, fp );
  if( n != SignatureSize ) {
  
    perror( "fread" );
    fclose( fp );
    return 0;
  }
  
  if( png_sig_cmp( signature, 0, SignatureSize ) != 0 ) {
    
    fclose( fp );
    return 0;
  }
  
  png_structp pngRead = png_create_read_struct( PNG_LIBPNG_VER_STRING, 0, 0, 0 );
  if( !pngRead ) {
    
    fclose( fp );
    return 0;
  }
  
  png_infop pngInfo = png_create_info_struct( pngRead );
  if( !pngInfo ) {
    
    png_destroy_read_struct( &pngRead, 0, 0 );
    fclose( fp );
    return 0;
  }

  uint8_t* data = 0;
  uint8_t** rows = 0;
  
  if( setjmp( png_jmpbuf( pngRead ) ) ) {
    
    if( data ) delete [] data;
    if( rows ) delete [] rows;
    
    png_destroy_read_struct( &pngRead, &pngInfo, 0 );
    fclose( fp );
    return 0;
  }
  
  png_init_io( pngRead, fp );
  png_set_sig_bytes( pngRead, SignatureSize );
  png_read_info( pngRead, pngInfo );
  
  uint32_t w = png_get_image_width( pngRead, pngInfo );
  uint32_t h = png_get_image_height( pngRead, pngInfo );
  uint32_t bitsPerChannel = png_get_bit_depth( pngRead, pngInfo );
  uint32_t channels = png_get_channels( pngRead, pngInfo );
  uint32_t colorType = png_get_color_type( pngRead, pngInfo );
  
  if( bitsPerChannel != 8 ||
      ( channels != 3 && channels != 4 ) ||
      ( colorType != PNG_COLOR_TYPE_RGB && colorType != PNG_COLOR_TYPE_RGB_ALPHA ) ) {
    
    png_destroy_read_struct( &pngRead, &pngInfo, 0 );
    fclose( fp );
    return 0;
  }
  
  if( (int) w > MaxTextureSize || (int) h > MaxTextureSize ) {
    
    png_destroy_read_struct( &pngRead, &pngInfo, 0 );
    fclose( fp );
    return 0;
  }
  
  data = new uint8_t[ w * h * channels ];
  rows = new uint8_t*[ h ];
  for( uint32_t i = 0; i < h; ++i )
    rows[ i ] = &data[ i * w * channels ];
  
  png_read_image( pngRead, rows );
  
  png_destroy_read_struct( &pngRead, &pngInfo, 0 );
  fclose( fp );
  
  delete [] rows;
  
  uint16_t* data16 = convertTexture( data, w, h, channels );
  delete [] data;
  
  glEnable(GL_TEXTURE_2D);
  
  GLuint textureId;
  glGenTextures( 1, &textureId );
  glBindTexture( GL_TEXTURE_2D, textureId );
  glTexImage2D( GL_TEXTURE_2D, 0, 
		( channels == 3 ) ? GL_RGB : GL_RGBA, w, h, 0, 
		( channels == 3 ) ? GL_RGB : GL_RGBA, 
		( channels == 3 ) ? GL_UNSIGNED_SHORT_5_6_5 : GL_UNSIGNED_SHORT_5_5_5_1, data16 );
  //GL_UNSIGNED_BYTE, data );
  glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
  glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
  
  //delete [] data;
  delete [] data16;
  
  return new Texture( filename, w, h, textureId );
}

Texture::Texture( const std::string& filename, int w, int h, GLuint id )
  : filename( filename ), width( w ), height( h ), id( id )
{
}

Texture::~Texture()
{
  // TODO...
}
