CXXFLAGS += -Wall -g 
CXXFLAGS += -O2
#CXXFLAGS += -O0
CXXFLAGS += $(shell sdl-config --cflags)
CXXFLAGS += -I/usr/include/GL
CXXFLAGS += -I../3rdparty/eigen
CXXFLAGS += $(shell pkg-config libpng --cflags)
#CXXFLAGS += -pg
CXXFLAGS += -DEIGEN_NO_DEBUG
CXXFLAGS += -DUSE_RENDERER=1
CXXFLAGS += -DUSE_PLATFORM=1

LDFLAGS += $(shell sdl-config --libs)
LDFLAGS += -lGL
LDFLAGS += -lGLU
LDFLAGS += -lm
LDFLAGS += $(shell pkg-config libpng --libs)
#LDFLAGS += -pg

BUILDDIR=build/pc

