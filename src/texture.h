#ifndef __TEXTURE_H__
#define __TEXTURE_H__

#include <string>

#include <gl.h>

struct Texture
{
  static Texture* load( const std::string& filename );

  Texture( const std::string& filename, int w, int h, GLuint id );
  ~Texture();
  
  std::string filename;
  int width;
  int height;
  GLuint id;
};

#endif // __TEXTURE_H__
