#ifndef _OPTIONS_H_
#define _OPTIONS_H_

// yaw: 232
// pit: -42
// pos: 43/72/37
// far: 100
// ./kleincraft -yaw 232 -pitch -42 -posx 43 -posy 72 -posz 37 -far 100

struct Options
{
  Options()
	: yaw(0)
	, pitch(0)
	, posx(0)
	, posy(0)
	, posz(0)
	, far(32)
  {
  }

  float yaw;
  float pitch;
  float posx;
  float posy;
  float posz;
  float far;
};

#endif /* _OPTIONS_H_ */

