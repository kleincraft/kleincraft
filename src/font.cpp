#include "font.h"
#include "renderer.h"
#include "texture.h"

#include <stdarg.h>
#include <stdio.h>

Font::Font( Texture* texture, 
	    int firstChar,
	    int numChars,
	    int charW,
	    int charH,
	    int advanceX,
	    int areaX,
	    int areaY,
	    int areaW,
	    int areaH,
	    int gapW,
	    int gapH )
  : _texture( texture ),
    _firstChar( firstChar ),
    _numChars( numChars ),
    _charW( charW ),
    _charH( charH ),
    _advanceX( advanceX )
{
  _characters = new Character[ numChars ];

  float x = areaX;
  float y = areaY;

  for( int i = 0; i < numChars; ++i ) {

    _characters[i].texCoords[0] = Eigen::Vector2f( x / _texture->width,
						   y / _texture->height );

    _characters[i].texCoords[1] = Eigen::Vector2f( x / _texture->width,
						   (y + charH) / _texture->height );

    _characters[i].texCoords[2] = Eigen::Vector2f( (x + charW) / _texture->width,
						   (y + charH) / _texture->height );

    _characters[i].texCoords[3] = Eigen::Vector2f( (x + charW) / _texture->width,
						   y / _texture->height );
    
    x += charW + gapW;
    if( x >= areaX + areaW ) {

      x = areaX;
      y += charH + gapH;
    }
  }
}

Font::~Font()
{
  delete [] _characters;
}

Eigen::Vector2f Font::printf( Renderer* renderer, const Eigen::Vector2f& pos0, const char* format, ... ) 
{
  if( !format ) return pos0;
  
  char buffer[ 4000 ];
  
  va_list list;
  va_start( list, format );
  vsnprintf( buffer, sizeof( buffer ), format, list );
  va_end( list );

  return print( renderer, pos0, buffer );
}

Eigen::Vector2f Font::print( Renderer* renderer, const Eigen::Vector2f& pos0, const char* text )
{
  FontRenderState renderState( renderer, _texture );
  renderState.setup();
  //renderState.init( RenderState::Mode2D, _texture->id );
  
  Eigen::Vector2f pos = pos0;
  
  int c;
  while( ( c = *text++ ) ) {
    
    int i = c - _firstChar;
    if( i >= 0 && i < _numChars ) {
      
      Character* character = &_characters[ i ];
      
      Eigen::Vector2f v[4];
      
      v[0] = Eigen::Vector2f(          pos[0],          pos[1] );
      v[1] = Eigen::Vector2f(          pos[0], pos[1] - _charH );
      v[2] = Eigen::Vector2f( pos[0] + _charW, pos[1] - _charH );
      v[3] = Eigen::Vector2f( pos[0] + _charW,          pos[1] );
      
      renderState.addRect2D( v, character->texCoords );
    }
    
    pos += Eigen::Vector2f( _advanceX, 0 );
  }
  
  renderState.flush();
  return pos;
}

