#ifndef __TERRAINLEAF_H__
#define __TERRAINLEAF_H__

#include <stdint.h>

class Octree;

struct TerrainLeaf
{
  void updateNeighborsObstruction( Octree* octree, 
								   const int x, 
								   const int y, 
								   const int z );

  void updateObstruction( Octree* octree, int x, int y, int z );

  void setBlockType( Octree* octree, int x, int y, int z, uint8_t bt );
  
  uint8_t blockType;
  uint8_t obstruction;
  uint8_t light;
  uint8_t _padding;
};

struct TerrainLeafCollision
{
  TerrainLeafCollision( TerrainLeaf* leaf, int x, int y, int z, float t0 /*, float t1*/ )
  : leaf(leaf), x(x), y(y), z(z), t0(t0) /*, _t1(t1)*/
  {
  }

  TerrainLeaf* leaf;
  int x;
  int y;
  int z;
  float t0;
  //float _t1;
};

struct TerrainLeafCollisionCmp
{
  bool operator()( const TerrainLeafCollision& c1, const TerrainLeafCollision& c2 ) const
  {
    return c1.t0 < c2.t0;
  }
};

#endif // __TERRAINLEAF_H__
