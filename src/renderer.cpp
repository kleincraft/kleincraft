#include "renderer.h"
#include "texture.h"

#include <SDL.h>

#include <gl.h>

#include <cassert>

#if( USE_RENDERER == RENDERER_GLES ) 
#include "gles11.h"
#endif

void Renderer::startFrame()
{
  _numTrisRendered = 0;
  glLoadIdentity();
}

void Renderer::renderFrame()
{
  #if USE_RENDERER == RENDERER_GL
  SDL_GL_SwapBuffers();
  #endif

  #if USE_RENDERER == RENDERER_GLES
  GLES11::swapBuffers();
  #endif
}

void Renderer::addNumTrisRendered( unsigned value )
{
  _numTrisRendered += value;
}

void Renderer::renderColoredCube( GLfloat x, GLfloat y, GLfloat z, GLfloat s, 
				  GLfloat r, GLfloat g, GLfloat b, GLfloat a )
{
  GLfloat buffer[ 12 * 3 * 3 ];

  GLfloat* ptr = buffer;

  // Front

  *ptr++ = x;
  *ptr++ = y + s;
  *ptr++ = z + s;

  *ptr++ = x;
  *ptr++ = y;
  *ptr++ = z + s;

  *ptr++ = x + s;
  *ptr++ = y;
  *ptr++ = z + s;

  *ptr++ = x + s;
  *ptr++ = y;
  *ptr++ = z + s;

  *ptr++ = x + s;
  *ptr++ = y + s;
  *ptr++ = z + s;

  *ptr++ = x;
  *ptr++ = y + s;
  *ptr++ = z + s;

  // Back

  *ptr++ = x + s;
  *ptr++ = y + s;
  *ptr++ = z;

  *ptr++ = x + s;
  *ptr++ = y;
  *ptr++ = z;

  *ptr++ = x;
  *ptr++ = y;
  *ptr++ = z;

  *ptr++ = x;
  *ptr++ = y;
  *ptr++ = z;

  *ptr++ = x;
  *ptr++ = y + s;
  *ptr++ = z;

  *ptr++ = x + s;
  *ptr++ = y + s;
  *ptr++ = z;

  // Top

  *ptr++ = x;
  *ptr++ = y + s;
  *ptr++ = z;

  *ptr++ = x;
  *ptr++ = y + s;
  *ptr++ = z + s;

  *ptr++ = x + s;
  *ptr++ = y + s;
  *ptr++ = z + s;

  *ptr++ = x + s;
  *ptr++ = y + s;
  *ptr++ = z + s;

  *ptr++ = x + s;
  *ptr++ = y + s;
  *ptr++ = z;

  *ptr++ = x;
  *ptr++ = y + s;
  *ptr++ = z;

  // Bottom

  *ptr++ = x;
  *ptr++ = y;
  *ptr++ = z + s;

  *ptr++ = x;
  *ptr++ = y;
  *ptr++ = z;

  *ptr++ = x + s;
  *ptr++ = y;
  *ptr++ = z;

  *ptr++ = x + s;
  *ptr++ = y;
  *ptr++ = z;

  *ptr++ = x + s;
  *ptr++ = y;
  *ptr++ = z + s;

  *ptr++ = x;
  *ptr++ = y;
  *ptr++ = z + s;

  // Right

  *ptr++ = x + s;
  *ptr++ = y + s;
  *ptr++ = z + s;

  *ptr++ = x + s;
  *ptr++ = y;
  *ptr++ = z + s;

  *ptr++ = x + s;
  *ptr++ = y;
  *ptr++ = z;

  *ptr++ = x + s;
  *ptr++ = y;
  *ptr++ = z;

  *ptr++ = x + s;
  *ptr++ = y + s;
  *ptr++ = z;

  *ptr++ = x + s;
  *ptr++ = y + s;
  *ptr++ = z + s;

  // Left

  *ptr++ = x;
  *ptr++ = y + s;
  *ptr++ = z;

  *ptr++ = x;
  *ptr++ = y;
  *ptr++ = z;

  *ptr++ = x;
  *ptr++ = y;
  *ptr++ = z + s;

  *ptr++ = x;
  *ptr++ = y;
  *ptr++ = z + s;

  *ptr++ = x;
  *ptr++ = y + s;
  *ptr++ = z + s;

  *ptr++ = x;
  *ptr++ = y + s;
  *ptr++ = z;

  glDisable( GL_TEXTURE_2D );
  glDisable( GL_LIGHTING );
  glDisable( GL_COLOR_MATERIAL );
  glEnable( GL_DEPTH_TEST );

  glColor4f( r, g, b, a );

  glDisableClientState( GL_NORMAL_ARRAY );
  glDisableClientState( GL_TEXTURE_COORD_ARRAY );
  glDisableClientState( GL_COLOR_ARRAY );

  glVertexPointer( 3, GL_FLOAT, 0, buffer );
  glDrawArrays( GL_TRIANGLES, 0, 12 * 3 );

  addNumTrisRendered( 12 );
}


RenderState::RenderState( Renderer* renderer )
  : _renderer( renderer )//,
//     _mode( Mode3DNormals ),
//     _texture( 0 )
{
  _bufferI = 0;
}

RenderState::~RenderState()
{
}

// void RenderState::init( Mode mode, GLuint texture )
// {
//   _mode = mode;
//   _texture = texture;

//   _vertexPtr = _vertices;
//   _texCoordPtr = _texCoords;
//   _normalPtr = _normals;
// }

void RenderState::flush()
{
  const unsigned numVertices = _bufferI / 8;
  if( !numVertices )
    return;
  
  //setup();

  //glDisableClientState( GL_INDEX_ARRAY );
  glDisableClientState( GL_COLOR_ARRAY );
  //glDisableClientState( GL_EDGE_FLAG_ARRAY );

  const unsigned bytesPerVertex = sizeof( GLfloat ) * 8;
  
  glVertexPointer( 3, GL_FLOAT, bytesPerVertex, &_buffer[0] );
  glTexCoordPointer( 2, GL_FLOAT, bytesPerVertex, &_buffer[3] );
  glNormalPointer( GL_FLOAT, bytesPerVertex, &_buffer[5] );

  glDrawArrays( GL_TRIANGLES, 0, numVertices );
  _renderer->addNumTrisRendered( numVertices / 3 );

  _bufferI = 0;
}

void RenderState::addRect( const Eigen::Vector3f* vertices, 
			   const Eigen::Vector2f* texCoords,
			   const Eigen::Vector3f* normal )
{
  if( !canAddRect() )
    flush();

  GLfloat* ptr = &_buffer[ _bufferI ];
  _bufferI += 48;

  // 1
  *ptr++ = vertices[0][0];
  *ptr++ = vertices[0][1];
  *ptr++ = vertices[0][2];

  *ptr++ = texCoords[0][0];
  *ptr++ = texCoords[0][1];

  *ptr++ = (*normal)[0];
  *ptr++ = (*normal)[1];
  *ptr++ = (*normal)[2];

  // 2
  *ptr++ = vertices[1][0];
  *ptr++ = vertices[1][1];
  *ptr++ = vertices[1][2];

  *ptr++ = texCoords[1][0];
  *ptr++ = texCoords[1][1];

  *ptr++ = (*normal)[0];
  *ptr++ = (*normal)[1];
  *ptr++ = (*normal)[2];

  // 3
  *ptr++ = vertices[2][0];
  *ptr++ = vertices[2][1];
  *ptr++ = vertices[2][2];

  *ptr++ = texCoords[2][0];
  *ptr++ = texCoords[2][1];

  *ptr++ = (*normal)[0];
  *ptr++ = (*normal)[1];
  *ptr++ = (*normal)[2];

  // 4
  *ptr++ = vertices[2][0];
  *ptr++ = vertices[2][1];
  *ptr++ = vertices[2][2];

  *ptr++ = texCoords[2][0];
  *ptr++ = texCoords[2][1];

  *ptr++ = (*normal)[0];
  *ptr++ = (*normal)[1];
  *ptr++ = (*normal)[2];

  // 5
  *ptr++ = vertices[3][0];
  *ptr++ = vertices[3][1];
  *ptr++ = vertices[3][2];

  *ptr++ = texCoords[3][0];
  *ptr++ = texCoords[3][1];

  *ptr++ = (*normal)[0];
  *ptr++ = (*normal)[1];
  *ptr++ = (*normal)[2];

  // 6
  *ptr++ = vertices[0][0];
  *ptr++ = vertices[0][1];
  *ptr++ = vertices[0][2];

  *ptr++ = texCoords[0][0];
  *ptr++ = texCoords[0][1];

  *ptr++ = (*normal)[0];
  *ptr++ = (*normal)[1];
  *ptr++ = (*normal)[2];
}

void RenderState::addRect2D( const Eigen::Vector2f* vertices, 
			     const Eigen::Vector2f* texCoords )
{
  if( !canAddRect() )
    flush();

  GLfloat* ptr = &_buffer[ _bufferI ];
  _bufferI += 48;

  // 1
  *ptr++ = vertices[0][0];
  *ptr++ = vertices[0][1];
  *ptr++ = 0.0f;

  *ptr++ = texCoords[0][0];
  *ptr++ = texCoords[0][1];

  *ptr++ = 0.0f;
  *ptr++ = 0.0f;
  *ptr++ = -1.0f;

  // 2
  *ptr++ = vertices[1][0];
  *ptr++ = vertices[1][1];
  *ptr++ = 0.0f;

  *ptr++ = texCoords[1][0];
  *ptr++ = texCoords[1][1];

  *ptr++ = 0.0f;
  *ptr++ = 0.0f;
  *ptr++ = -1.0f;

  // 3
  *ptr++ = vertices[2][0];
  *ptr++ = vertices[2][1];
  *ptr++ = 0.0f;

  *ptr++ = texCoords[2][0];
  *ptr++ = texCoords[2][1];

  *ptr++ = 0.0f;
  *ptr++ = 0.0f;
  *ptr++ = -1.0f;

  // 4
  *ptr++ = vertices[2][0];
  *ptr++ = vertices[2][1];
  *ptr++ = 0.0f;

  *ptr++ = texCoords[2][0];
  *ptr++ = texCoords[2][1];

  *ptr++ = 0.0f;
  *ptr++ = 0.0f;
  *ptr++ = -1.0f;

  // 5
  *ptr++ = vertices[3][0];
  *ptr++ = vertices[3][1];
  *ptr++ = 0.0f;

  *ptr++ = texCoords[3][0];
  *ptr++ = texCoords[3][1];

  *ptr++ = 0.0f;
  *ptr++ = 0.0f;
  *ptr++ = -1.0f;

  // 6
  *ptr++ = vertices[0][0];
  *ptr++ = vertices[0][1];
  *ptr++ = 0.0f;

  *ptr++ = texCoords[0][0];
  *ptr++ = texCoords[0][1];

  *ptr++ = 0.0f;
  *ptr++ = 0.0f;
  *ptr++ = -1.0f;
}

bool Renderer::init()
{
  if( SDL_Init( SDL_INIT_VIDEO ) != 0 ) {

    return false;
  }

  atexit( SDL_Quit );

  if( !initScreen() )
    return false;

  glViewport( 0, 0, 320, 240 );

  return true;
}

#if( USE_RENDERER == RENDERER_GL )
bool Renderer::initScreen()
{
  SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

  if( !SDL_SetVideoMode( 320, 240, 0, SDL_OPENGL ) )
    return false;

  return true;
}

void Renderer::shutdown()
{
}
#endif

#if( USE_RENDERER == RENDERER_GLES )
bool Renderer::initScreen()
{
  SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 5 );
  SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 6 );
  SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 5 );
  SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

  if( !SDL_SetVideoMode( 320, 240, 16, SDL_SWSURFACE ) )
    return false;
 
  if( !GLES11::initGLES() )
    return false;
 
  return true;
}

void Renderer::shutdown()
{
  GLES11::shutdownGLES();
}
#endif

TerrainRenderState::TerrainRenderState( Renderer* renderer, Texture* texture )
  : RenderState( renderer ), 
    _textureId( texture->id )
{
}

void TerrainRenderState::setup()
{
  glShadeModel( GL_FLAT );
  //glColor4f( 1, 1, 1, 1 );

  glDisable( GL_ALPHA_TEST );
  glDisable( GL_BLEND );
  glEnable( GL_CULL_FACE );
  glEnable( GL_DEPTH_TEST );
  
  glEnable( GL_TEXTURE_2D );
  glBindTexture( GL_TEXTURE_2D, _textureId );
  
  glEnableClientState( GL_VERTEX_ARRAY );
  glEnableClientState( GL_TEXTURE_COORD_ARRAY );
  glEnableClientState( GL_NORMAL_ARRAY );
}

FontRenderState::FontRenderState( Renderer* renderer, Texture* texture )
  : RenderState( renderer ),
    _textureId( texture->id )
{
}

void FontRenderState::setup()
{
  glShadeModel( GL_FLAT );
  glColor4f( 1, 1, 1, 1 );

  glDisable( GL_ALPHA_TEST );
  glDisable( GL_BLEND );
  glEnable( GL_CULL_FACE );
  glDisable( GL_DEPTH_TEST );
  
  glEnable( GL_TEXTURE_2D );
  glBindTexture( GL_TEXTURE_2D, _textureId );
  
  glEnableClientState( GL_VERTEX_ARRAY );
  glEnableClientState( GL_TEXTURE_COORD_ARRAY );
  glDisableClientState( GL_NORMAL_ARRAY );
}
