#include "camera.h"
#include "octree.h"
#include "renderer.h"
#include "terrain.h"
#include "terraingenerator.h"

#include <simplexnoise1234.h>

#include <gl.h>

#include <limits.h>

#include <algorithm>


NonPagedTerrain::NonPagedTerrain( int depth, AbstractTerrainGenerator* generator )
  : _generator(generator)
{
  _octree = new Octree( depth );
}

NonPagedTerrain::~NonPagedTerrain()
{
  delete _octree;
}

bool NonPagedTerrain::init()
{
  _generator->generate( _octree, 0, 0, 0 );
  return true;
}

void NonPagedTerrain::shutdown()
{
}

void NonPagedTerrain::update( const Vector3f& pos )
{
}

void NonPagedTerrain::frustumCull( Camera* camera )
{
  int tmp;
  _octree->frustumCull( camera, 0, 0, 0, &tmp );
}

void NonPagedTerrain::render( RenderState* renderState )
{
  GLfloat lightPos[3] = { 0, (GLfloat) _octree->getSize(), 0 };
  glLightfv( GL_LIGHT0, GL_POSITION, lightPos );
  
  GLfloat lightAmbient[4] = {0,0,0,1};
  glLightfv( GL_LIGHT0, GL_AMBIENT, lightAmbient );
  GLfloat lightDiffuse[4] = {1,1,1,1};
  glLightfv( GL_LIGHT0, GL_AMBIENT, lightDiffuse );
  GLfloat lightSpecular[4] = {1,1,1,1};
  glLightfv( GL_LIGHT0, GL_SPECULAR, lightSpecular );
  
  GLfloat materialSpecular[4] = {1,1,1,1};
  glMaterialfv( GL_FRONT_AND_BACK, GL_SPECULAR, materialSpecular );
  GLfloat materialEmission[4] = {0,0,0,1};
  glMaterialfv( GL_FRONT_AND_BACK, GL_SPECULAR, materialEmission );
  
  glColor4f(0.8,0.8,0.8,1.0);
  
  //glEnable( GL_LIGHTING );
  //glEnable( GL_LIGHT0 );
  //glEnable( GL_COLOR_MATERIAL );
  
  renderState->setup();

  _octree->render( renderState, 0, 0, 0 );

  renderState->flush();
  
  //glDisable( GL_LIGHTING );
}

void NonPagedTerrain::castRay( const Vector3f& pos, const Vector3f& dir, 
			       Octree** octree, TerrainLeaf** leaf, int* lx, int* ly, int* lz, /*int *face,*/ float* d )
{
  _octree->castRay( pos, dir, 0, 0, 0,
		    leaf, lx, ly, lz, /*face,*/ d );

  *octree = _octree;
}

void NonPagedTerrain::testCollision( const AABox& boxIn, const Vector3f& dpIn, 
				     std::vector< TerrainLeafCollision >* collisionsOut )
{
  _octree->testCollision( boxIn, dpIn, 
			  0, 0, 0, 
			  collisionsOut );
}

int NonPagedTerrain::getSizeX() const
{
  return _octree->getSize();
}

int NonPagedTerrain::getSizeY() const
{
  return _octree->getSize();
}

int NonPagedTerrain::getSizeZ() const
{
  return _octree->getSize();
}


PagedTerrain::PagedTerrain( int depth, AbstractTerrainGenerator* generator )
  : _depth( depth ), _thread( generator )
{
  _pageSize = 1 << depth;

  for( int i = 0; i < NumPages; ++i )
    _pages[i] = new TerrainPage;
}

PagedTerrain::~PagedTerrain()
{
  for( int i = 0; i < NumPages; ++i )
    delete _pages[i];
}

bool PagedTerrain::init()
{
  _thread.start();

  _thread.beginSend();

  int l = 0;
  for( int k = 0; k < NumPagesH; ++k ) {
    for( int j = 0; j < NumPagesV; ++j ) {
      for( int i = 0; i < NumPagesH; ++i ) {
	
	TerrainPage* page = _pages[l++];

	page->state = TerrainPage::AboutToBeSent;
	page->next_ox = page->curr_ox = i * getPageSize();
	page->next_oy = page->curr_oy = j * getPageSize();
	page->next_oz = page->curr_oz = k * getPageSize();

	_thread.sendGeneratePage( page, _depth );
      }
    }
  }

  _thread.endSend();

  return true;
}

void PagedTerrain::shutdown()
{
  _thread.stop();
}

void PagedTerrain::frustumCull( Camera* camera )
{
  for( int i = 0; i < NumPagesH * NumPagesH * NumPagesV; ++i ) {
    
    _pages[i]->frustumCull( camera );
  }
}

void PagedTerrain::update( const Vector3f& pos )
{
  updatePageStates();
  requestNewPages( pos );
}

void PagedTerrain::updatePageStates()
{
  for( int i = 0; i < NumPages; ++i ) {
    
    unsigned state = _pages[i]->state;
    switch( state ) {
      
    case TerrainPage::Returned:
      //fprintf( stderr, "Page activated: %d/%d/%d\n", pages[i]->ox, pages[i]->oy, pages[i]->oz );
      _pages[i]->state = TerrainPage::Active;
      break;
      
    default:
      break;
    }
  }
}

void PagedTerrain::sortPages()
{
  std::sort( &_pages[0], &_pages[NumPages], PageCmp() );
}

void PagedTerrain::requestNewPages( const Vector3f& pos )
{
  int px = floorf( pos[0] );
  int py = floorf( pos[1] );
  int pz = floorf( pos[2] );
  
  int dx = px - _wx;
  int dy = py - _wy;
  int dz = pz - _wz;

  int newWx0 = _wx;
  int newWy0 = _wy;
  int newWz0 = _wz;

  const int Threshold = ( getPageSize() * 5 ) / 4;
  const int MaxSizeH = (1<<20) - getPageSize() * NumPagesH;
  const int MaxSizeV = (1<<10) - getPageSize() * NumPagesV;

  if( dx < Threshold ) {

    newWx0 = std::max( 0, _wx - 2 * getPageSize() );
  }
  else if( dx >= (NumPagesH * getPageSize() - Threshold) ) {

    newWx0 = std::min( MaxSizeH, _wx + 2 * getPageSize() );
  }
  if( dy < Threshold ) {

    newWy0 = std::max( 0, _wy - 2 * getPageSize() );
  }
  else if( dy >= (NumPagesH * getPageSize() - Threshold) ) {

    newWy0 = std::min( MaxSizeV, _wy + 2 * getPageSize() );
  }
  if( dz < Threshold ) {

    newWz0 = std::max( 0, _wz - 2 * getPageSize() );
  }
  else if( dz >= (NumPagesH * getPageSize() - Threshold) ) {

    newWz0 = std::min( MaxSizeH, _wz + 2 * getPageSize() );
  }

  int newWx1 = newWx0 + NumPagesH * getPageSize();
  int newWy1 = newWy0 + NumPagesV * getPageSize();
  int newWz1 = newWz0 + NumPagesH * getPageSize();

  TerrainPage* pages1[ NumPages ];
  TerrainPage* pages2[ NumPages ];
  memset( pages1, 0, sizeof(pages1) );
  memset( pages2, 0, sizeof(pages2) );

//   for( int i = 0; i < NumPages; ++i ) {

//     TerrainPage* page = _pages[i];
//     if( !page->isCurrent() ) {
      
//       int pdx = page->next_ox - newWx0;
//       if( pdx < 0 ) {

//       }
//       else {

// 	int pi = pdx/getPageSize();
//       }
//     }
//     else {

      
//     }
//   }
  
  

  sortPages();

  _thread.beginSend();
  
  for( int i = 0; i < NumPages; ++i ) {

//     if( _pages[i]->state != TerrainPage::MarkedForGenerating ) {

//       _pages[i]->state = TerrainPage::Generating;
//       _thread.sendGeneratePage( _pages[i], _depth );
//    }
  }

  _thread.endSend();
}

void PagedTerrain::markPage( int i, int j, int k, int dx, int dz )
{
//   TerrainPage* page = _pages[ k * NumPagesV * NumPagesH + 
// 			     j * NumPagesV + 
// 			     i ];
//   page->ox += dx;
//   page->oz += dz;
//   page->state = TerrainPage::MarkedForGenerating;
}

bool PagedTerrain::getWindowPos( int* x, int* z ) const
{
  *x = INT_MAX;
  *z = INT_MAX;

  for( int i = 0; i < NumPages; ++i ) {

//     *x = std::min( *x, pages[i]->ox );
//     *z = std::min( *z, pages[i]->oz );
  }
  
  return (*x != INT_MAX) && (*z != INT_MAX);
}

void PagedTerrain::render( RenderState* renderState )
{
  //  glPushMatrix();
  //glLoadIdentity();

//   glShadeModel( GL_FLAT );
//   glDisable( GL_TEXTURE_2D );
//   glDisable( GL_BLEND );
//   glColor4f( 1, 0, 0, 1 );
//   glEnable( GL_CULL_FACE );

//   TerrainRenderState renderState( renderer, texture );
//   renderState.setup();
  
  GLfloat lightPos[3] = { 0, (GLfloat)(NumPagesV * getPageSize()), 0 };
  glLightfv( GL_LIGHT0, GL_POSITION, lightPos );
  
  GLfloat lightAmbient[4] = {0,0,0,1};
  glLightfv( GL_LIGHT0, GL_AMBIENT, lightAmbient );
  GLfloat lightDiffuse[4] = {1,1,1,1};
  glLightfv( GL_LIGHT0, GL_AMBIENT, lightDiffuse );
  GLfloat lightSpecular[4] = {1,1,1,1};
  glLightfv( GL_LIGHT0, GL_SPECULAR, lightSpecular );
  
  GLfloat materialSpecular[4] = {1,1,1,1};
  glMaterialfv( GL_FRONT_AND_BACK, GL_SPECULAR, materialSpecular );
  GLfloat materialEmission[4] = {0,0,0,1};
  glMaterialfv( GL_FRONT_AND_BACK, GL_SPECULAR, materialEmission );
  
  glColor4f(0.8,0.8,0.8,1.0);
  
  glEnable( GL_LIGHTING );
  glEnable( GL_LIGHT0 );
  glEnable( GL_COLOR_MATERIAL );
  
  renderState->setup();
  
  //renderState.init( RenderState::Mode3DNormals, texture->id );
  
  for( int i = 0; i < NumPages; ++i ) {
    
    _pages[i]->render( renderState );
  }
  
  //renderState.flush();
  
  renderState->flush();
  
  glDisable( GL_LIGHTING );
  
  //  glPopMatrix();
}
