#ifndef __TERRAIN_H__
#define __TERRAIN_H__

#include "terrainpage.h"
#include "terrainthread.h"

#include <Eigen/Core>
using namespace Eigen;

class AAbox;
class AbstractTerrainGenerator;
class Camera;
class Octree;
class TerrainPage;

class AbstractTerrain 
{
 public:
  
  virtual ~AbstractTerrain() {}

  virtual bool init() = 0;
  virtual void shutdown() = 0;

  //! Updates terrain according to camera position...
  virtual void update( const Vector3f& pos ) = 0;

  //! Performs frustum culling
  virtual void frustumCull( Camera* camera ) = 0;

  //! Renders terrain
  virtual void render( RenderState* renderState ) = 0;

  virtual void castRay( const Vector3f& pos, const Vector3f& dir, 
			Octree** octree, TerrainLeaf** leaf, int* lx, int* ly, int* lz, /*int *face,*/ float* d ) = 0;

  virtual int getSizeX() const = 0;
  virtual int getSizeY() const = 0;
  virtual int getSizeZ() const = 0;

  virtual void testCollision( const AABox& boxIn, const Vector3f& dpIn, 
			      std::vector< TerrainLeafCollision >* collisionsOut ) = 0;
};

class NonPagedTerrain : public AbstractTerrain
{
 public:
  NonPagedTerrain( int depth, AbstractTerrainGenerator* generator );
  ~NonPagedTerrain();

  bool init();
  void shutdown();
  void update( const Vector3f& pos );
  void frustumCull( Camera* camera );
  void render( RenderState* renderState );

  void castRay( const Vector3f& pos, const Vector3f& dir, 
		Octree** octree, TerrainLeaf** leaf, int* lx, int* ly, int* lz, /*int *face,*/ float* d );

  void testCollision( const AABox& boxIn, const Vector3f& dpIn, 
		      std::vector< TerrainLeafCollision >* collisionsOut );
  
  int getSizeX() const;
  int getSizeY() const;
  int getSizeZ() const;

 private:
  AbstractTerrainGenerator* _generator;
  Octree* _octree;
};

class PagedTerrain : public AbstractTerrain
{
 public:

  static const int NumPagesV = 2;
  static const int NumPagesH = 4;
  static const int NumPages = NumPagesH * NumPagesH * NumPagesV;

  PagedTerrain( int depth, AbstractTerrainGenerator* generator );
  ~PagedTerrain();

  bool init();
  void shutdown();

  //! Updates terrain according to camera position...
  void update( const Vector3f& pos );

  //! Performs frustum culling
  void frustumCull( Camera* camera );

  //! Renders terrain
  void render( RenderState* renderState );

  void castRay( const Vector3f& pos, const Vector3f& dir, 
		Octree** octree, TerrainLeaf** leaf, int* lx, int* ly, int* lz, /*int *face,*/ float* d ) 
  {
    // FIXME
  }

  int getPageSize() const { return _pageSize; }

//   int getSizeX() const;
//   int getSizeY() const;
//   int getSizeZ() const;

 private:

  struct PageCmp
  {
    bool operator()( const TerrainPage* p1, const TerrainPage* p2 ) const
    {
      int dz = p2->next_oz - p1->next_oz;
      if( dz < 0 )
	return false;
      else if( dz > 0 )
	return true;

      int dy = p2->next_oy - p1->next_oy;
      if( dy < 0 )
	return false;
      else if( dy > 0 )
	return true;
      
      int dx = p2->next_ox - p1->next_ox;
      if( dx < 0 )
	return false;
      else if( dx > 0 )
	return true;

      return false;
    }
  };

  //! Processed pages returned from the terrain thread
  void updatePageStates();

  //! Sort pages by coordinates
  void sortPages();

  //! Requests new pages from the terrain thread
  void requestNewPages( const Vector3f& pos );

  //! Prepares a page for a generate request
  void markPage( int i, int j, int k, int dx, int dy );

  //! Gets the current position of the horizontal window
  //! Returns false if the window is invalid.
  bool getWindowPos( int* x, int* y ) const;

  TerrainPage* _pages[ NumPagesH * NumPagesH * NumPagesV ];

  int _depth;
  int _pageSize;
  
  int _wx;
  int _wy;
  int _wz;

  TerrainThread _thread;
};

#endif // __TERRAIN_H__
