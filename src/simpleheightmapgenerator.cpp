#include "octree.h"
#include "simpleheightmapgenerator.h"

#include <simplexnoise1234.h>

#include <algorithm>

SimpleHeightMapGenerator::SimpleHeightMapGenerator( int totalH )
  : totalH(totalH)
{
}

void SimpleHeightMapGenerator::generate( Octree* octree, int ox, int oy, int oz )
{
  const int s = octree->getSize();
  //const int totalH = s * Terrain::NumPagesV;

  //delete page->octree;
  //page->octree = new Octree( depth );
  
  octree->clear();

  for( int z = 0; z < s; ++z ) {
    for( int x = 0; x < s; ++x ) {
      
      const float Frequency = 1.0f / 200.0f;
      
      int h = std::max( 1, (int)( ( SimplexNoise1234::noise( Frequency * ( x + ox ), 
							     Frequency * ( z + oz ) ) + 1.0f ) * 0.5f * totalH ) ) - oy;

      if( h > s )
	h = s;

//       if( page->oz == 0 && z == 15 ||
// 	  page->oz == 8 && z == 0 ) {
//
// 	fprintf( stderr, "%d %d %d %d\n", x, z, page->oy, h );
//       }
      
      for( int y = 0; y < h; ++y ) {
	
	TerrainLeaf* leaf = octree->getLeaf( x, y, z ); // FIXME: Faster way to iterate...
	if( leaf ) {
	  
	  leaf->blockType = 1;
	}
      }
    }
  }
  
  octree->updateObstruction();
  //page->state = TerrainPage::Returned;
}
