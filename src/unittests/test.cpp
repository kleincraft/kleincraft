#include "test.h"

#include <memory>
#include <iostream>
#include <vector>

std::vector<AbstractTestFactory*>& getTestFactories()
{
  static std::vector<AbstractTestFactory*> s;
  return s;
}

AbstractTestFactory::AbstractTestFactory(const char* name)
  : _name(name)
{
  getTestFactories().push_back(this);
}

const std::string& AbstractTestFactory::getName() const { return _name; }

int main( int argc, char** argv )
{
  int numFailed = 0;
  for( unsigned i = 0; i < getTestFactories().size(); ++i ) {

    std::cerr << "Test #" << i << ": " << getTestFactories()[i]->getName() << '\n';
    std::auto_ptr<AbstractTest> test( getTestFactories()[i]->create());

    if( !test.get()->run() ) {

      std::cerr << "FAILED: Test #" << i << '\n';
      numFailed++;
    }
    else {
      
      std::cerr << "SUCCESS: Test #" << i << '\n';
    }
  }

  return numFailed;
}
