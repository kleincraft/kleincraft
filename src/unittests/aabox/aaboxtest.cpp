#include "test.h"
#include "../../aabox.h"

#define TEST_ASSERT( cond, mesg ) if( !( cond ) ) { std::cerr << "FAIL: " << mesg << '\n'; return false; } else { std::cerr << "PASS: " << mesg << '\n'; }

struct AABoxTest : public AbstractTest
{
  bool run() 
  { 
    float t0, t1;

    {
      AABox box1( Vector3f( 0.0f, 0.0f, 0.0f ), 1.0f );
      AABox box2( Vector3f( 3.0f, 0.0f, 0.0f ), 1.0f );
      
      bool res1 = box1.sweep( box2, Vector3f( 3.0f, 0.0f, 0.0f ), &t0, &t1 );
      //std::cerr << res1 << '/' << t0 << '/' << t1 << '\n';
      
      bool res2 = box2.sweep( box1, Vector3f( 3.0f, 0.0f, 0.0f ), &t0, &t1 );
      //std::cerr << res2 << '/' << t0 << '/' << t1 << '\n';
      
      TEST_ASSERT( !res1, "sweep away");
      TEST_ASSERT( res2, "sweep into" );
      
      bool res3 = box1.sweep( box2, Vector3f( 0, 1, 0 ), &t0, &t1 );
      TEST_ASSERT( !res3, "sweep parallel");
      
      bool res4 = box1.sweep( box2, Vector3f( 0, 0, 0 ), &t0, &t1 );
      TEST_ASSERT( !res4, "sweep no motions");

      bool res5 = box2.sweep( box1, Vector3f( 1, 0, 0 ), &t0, &t1 );
      TEST_ASSERT( !res5, "sweep short motion");
    }
    
    {

      AABox box1( Vector3f( 0, 0, 0 ), 2 );
      AABox box2( Vector3f( 1, 0, 0 ), 2 );

      TEST_ASSERT( box1.sweep( box2, Vector3f(0,0,0), &t0, &t1), "overlapping no motion" );

      TEST_ASSERT( box1.sweep( box2, Vector3f(1,0,0), &t0, &t1), "overlapping x motion" );
      TEST_ASSERT( box1.sweep( box2, Vector3f(0,1,0), &t0, &t1), "overlapping y motion" );
      TEST_ASSERT( box1.sweep( box2, Vector3f(0,0,1), &t0, &t1), "overlapping z motion" );

      TEST_ASSERT( box1.sweep( box2, Vector3f(-1,0,0), &t0, &t1), "overlapping -x motion" );
      TEST_ASSERT( box1.sweep( box2, Vector3f(0,-1,0), &t0, &t1), "overlapping -y motion" );
      TEST_ASSERT( box1.sweep( box2, Vector3f(0,0,-1), &t0, &t1), "overlapping -z motion" );
    }

    return true; 
  }
};

DECLARE_TEST(AABoxTest);
