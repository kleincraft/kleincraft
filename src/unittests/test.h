#ifndef __TEST_H__
#define __TEST_H__

#include <string>
#include <vector>

#define DECLARE_TEST(cls) static TestFactory< cls > _factory(#cls)

struct AbstractTest 
{
  virtual bool run() = 0;
};

class AbstractTestFactory
{
 public:
  AbstractTestFactory( const char* name );

  const std::string& getName() const;

  virtual AbstractTest* create() = 0;

 private:
  std::string _name;
};

template< class TestClass >
struct TestFactory : public AbstractTestFactory
{
  TestFactory( const char* name )
    : AbstractTestFactory( name )
  {
  }
  
  AbstractTest* create() { return new TestClass; }
};

#endif // __TEST_H__
