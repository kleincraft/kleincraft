vpath %.cpp ../..:..

TEST_DIR=$(CURDIR)

#SOURCES += $(TEST_DIR)/../test.cpp
SOURCES += test.cpp
OBJECTS=$(patsubst %.cpp, $(TEST_DIR)/%.o, $(SOURCES))

TARGET=$(TEST_DIR)/test

CXXFLAGS += -I$(TEST_DIR)/..

$(TARGET): $(OBJECTS)
	$(CXX) -o $@ $(OBJECTS) $(LDFLAGS)

$(TEST_DIR)/%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<
