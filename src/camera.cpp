#include "camera.h"
#include "math.h"
#include "renderer.h"

#include <Eigen/Geometry>

#include <stdio.h>

#include <iostream>

//#define USE_GLU
#undef USE_GLU
#ifdef USE_GLU
#include <glu.h>
#endif

Camera::Camera()
  : _dirty(true)
{
}

void Camera::setupFrustum( GLfloat fov, GLfloat ratio, GLfloat nearD, GLfloat farD )
{
  _frustumFov = fov;
  _frustumRatio = ratio;

  _nearD = nearD;
  _nearH = tanf( Math::degToRad( fov ) * 0.5f ) * nearD;
  _nearW = _nearH * ratio;
  
  _farD = farD;
  _farH = tanf( Math::degToRad( fov ) * 0.5f ) * farD;
  _farW = _farH * ratio;
}

void Camera::applyFrustum()
{
  glPushMatrix();
  glMatrixMode( GL_PROJECTION );
  glLoadIdentity();

  #if USE_RENDERER == RENDERER_GLES
  glFrustumf( -_frustumRatio * _nearH, _frustumRatio * _nearH,
	      -_nearH, _nearH,
	      _nearD, _farD );
  #else

  #ifdef USE_GLU
  gluPerspective( _frustumFov, _frustumRatio, _nearD, _farD );
  
  #else
  glFrustum( -_frustumRatio * _nearH, _frustumRatio * _nearH,
	     -_nearH, _nearH,
	     _nearD, _farD );
  #endif

  #endif
  
  glMatrixMode( GL_MODELVIEW );
  glPopMatrix();
}

void Camera::set( const Vector3f& pos, float yaw, float pitch )
{
  _pos = pos;

  _yaw = Math::degToRad( yaw );
  _pitch = Math::degToRad( pitch );

  calcDir();
  //calcFrustum();
  _dirty = true;
}

void Camera::move( float dX, float dY, float dZ, float dYaw, float dPitch )
{
  _yaw += Math::degToRad( dYaw );
  _pitch += Math::degToRad( dPitch );

  calcDir();

  _pos += _dir * dZ;
  _pos += _up * dY;
  _pos += _right * dX;

  //_pos += Vector3f( dY, dY, dZ );
  
  //calcFrustum();
  _dirty = true;
}

void Camera::applyCamera()
{
  #ifdef USE_GLU

  Vector3f center = _pos + _dir;
  gluLookAt( _pos[0], _pos[1], _pos[2], center[0], center[1], center[2], _up[0], _up[1], _up[2] );

  #else

  Vector3f side = _dir.cross( _up ).normalized();
  Vector3f up = side.cross( _dir );
    
  GLfloat matrix[16];

  matrix[ 0] = side[0];
  matrix[ 4] = side[1];
  matrix[ 8] = side[2];
  matrix[12] = 0.0f;

  matrix[ 1] = up[0];
  matrix[ 5] = up[1];
  matrix[ 9] = up[2];
  matrix[13] = 0.0f;

  matrix[ 2] = -_dir[0];
  matrix[ 6] = -_dir[1];
  matrix[10] = -_dir[2];
  matrix[14] = 0.0f;

  matrix[ 3] = 0.0f;
  matrix[ 7] = 0.0f;
  matrix[11] = 0.0f;
  matrix[15] = 1.0f;

  glMultMatrixf( matrix );

  glTranslatef( -_pos[0], -_pos[1], -_pos[2] );
  #endif
}

bool Camera::isSphereInFrustum( const Vector3f& pos, float radius ) //const
{
  calcFrustum();

  for( int i = 0; i < 6; ++i ) {

    float d = _frustumD[i] + _frustumN[i].dot( pos );
    if( d < -radius )
      return false;
    else if( d < radius )
      return true;
  }
  
  return true;
}

bool Camera::isAACubeInFrustum( const Vector3f& pos, float size ) //const 
{
  calcFrustum();

  const Vector3f corners[8] = { 
    pos, 
    pos + Vector3f( size,    0,    0 ),
    pos + Vector3f(    0, size,    0 ),
    pos + Vector3f( size, size,    0 ),
    pos + Vector3f(    0,    0, size ),
    pos + Vector3f( size,    0, size ),
    pos + Vector3f(    0, size, size ),
    pos + Vector3f( size, size, size )
  };
  
  for( int i = 0; i < 6; ++i ) {
    
    unsigned numAbove = 8;
    for( int j = 0; j < 8; ++j ) {
      
      float d = _frustumD[i] + _frustumN[i].dot( corners[j] );
      if( d < 0 )
	numAbove--;
    }
    
    if( !numAbove )
      return false;
  }

  return true;
}

float Camera::getYaw() const
{
  return Math::radToDeg( _yaw );
}

float Camera::getPitch() const
{
  return Math::radToDeg( _pitch );
}

static void makePlane( Vector3f* p, Vector3f* n, float* d,
		       const Vector3f& v1, const Vector3f& v2, const Vector3f& v3 )
{
  Vector3f aux1 = v1 - v2;
  Vector3f aux2 = v3 - v2;
  
  *n = aux2.cross( aux1 ).normalized();

  *p = v2;
  *d = -( n->dot( *p ) );
}

void Camera::calcFrustum()
{
  // http://www.lighthouse3d.com/opengl/viewfrustum/

//   Vector3f farCenter = _pos + _dir * _farD;
//   Vector3f farTopLeft = farCenter + (_up * _farH * 0.5f) - (_right * _farW * 0.5f);
//   Vector3f farBottomLeft = farCenter - (_up * _farH * 0.5f) - (_right * _farW * 0.5f);
//   Vector3f farBottomRight = farCenter - (_up * _farH * 0.5f) + (_right * _farW * 0.5f);
  
//   Vector3f nearCenter = _pos + _dir * _nearD;
//   Vector3f nearTopLeft = nearCenter + (_up * _nearH * 0.5f) - (_right * _nearW * 0.5f);
//   Vector3f nearBottomLeft = nearCenter - (_up * _nearH * 0.5f) - (_right * _nearW * 0.5f);
//   Vector3f nearBottomRight = nearCenter - (_up * _nearH * 0.5f) + (_right * _nearW * 0.5f);
  
//   _frustumP[ NearPlane ] = nearCenter;
//   _frustumN[ NearPlane ] = _dir;
  
//   _frustumP[ FarPlane ] = farCenter;
//   _frustumN[ FarPlane ] = _dir * -1.0f;
  
//   Vector3f a1 = nearCenter + _right * _nearW * 0.5f; // near mid-right
//   Vector3f a2 = (a1 - _pos);
//   _frustumP[ RightPlane ] = a1;
//   _frustumN[ RightPlane ] = _up.cross( a2 ).normalized();
  
//   a1 = nearCenter - _right * _nearW * 0.5f; // near mid-left
//   a2 = (a1 - _pos);
//   _frustumP[ LeftPlane ] = a1;
//   _frustumN[ LeftPlane ] = _up.cross( a2 ).normalized() * -1.0f;
  
//   a1 = nearCenter + _up * _nearH * 0.5f; // near mid-top
//   a2 = (a1 - _pos);
//   _frustumP[ TopPlane ] = a1;
//   _frustumN[ TopPlane ] = _right.cross( a2 ).normalized() * -1.0f;
  
//   a1 = nearCenter - _up * _nearH * 0.5f; // near mid-bottom
//   a2 = (a1 - _pos);
//   _frustumP[ BottomPlane ] = a1;
//   _frustumN[ BottomPlane ] = _right.cross( a2 ).normalized();

//   for( int i = 0; i < 6; ++i ) {

//     _frustumD[i] = -( _frustumN[i].dot( _frustumP[i] ) );
//   }

  if( !_dirty )
    return;

  _dirty = false;

  Vector3f center = _pos + _dir;
  Vector3f Z = _pos - center;
  Z.normalize();

  Vector3f X = _up.cross(Z);
  X.normalize();
  
  Vector3f Y = Z.cross(X);

  Vector3f nc = _pos - Z * _nearD;
  Vector3f fc = _pos - Z * _farD;

  Vector3f ntl = nc + Y * _nearH - X * _nearW;
  Vector3f ntr = nc + Y * _nearH + X * _nearW;
  Vector3f nbl = nc - Y * _nearH - X * _nearW;
  Vector3f nbr = nc - Y * _nearH + X * _nearW;

  Vector3f ftl = fc + Y * _farH - X * _farW;
  Vector3f ftr = fc + Y * _farH + X * _farW;
  Vector3f fbl = fc - Y * _farH - X * _farW;
  Vector3f fbr = fc - Y * _farH + X * _farW;

  makePlane( &_frustumP[TopPlane], &_frustumN[TopPlane], &_frustumD[TopPlane],
	     ntr, ntl, ftl );
  makePlane( &_frustumP[BottomPlane], &_frustumN[BottomPlane], &_frustumD[BottomPlane],
	     nbl, nbr, fbr );
  makePlane( &_frustumP[LeftPlane], &_frustumN[LeftPlane], &_frustumD[LeftPlane],
	     ntl, nbl, fbl );
  makePlane( &_frustumP[RightPlane], &_frustumN[RightPlane], &_frustumD[RightPlane],
	     nbr, ntr, ftr );
  makePlane( &_frustumP[NearPlane], &_frustumN[NearPlane], &_frustumD[NearPlane],
	     ntl, ntr, nbr );
  makePlane( &_frustumP[FarPlane], &_frustumN[FarPlane], &_frustumD[FarPlane],
	     fbr, ftl, fbl );

//   fprintf( stderr, "{\n" );
//   for( int i = 0; i < 6; ++i )
//     fprintf( stderr, "[%.1f, %.1f, %.1f] [%.1f, %.1f, %.1f] \n", _frustumP[i][0], _frustumP[i][1], _frustumP[i][2], _frustumN[i][0], _frustumN[i][1], _frustumN[i][2] );
//   fprintf( stderr, "}\n" );
}

void Camera::normalizeYawPitch()
{
  while( _yaw < 0 )
    _yaw += Math::PI * 2.0f;
  while( _yaw >= Math::PI * 2.0f )
    _yaw -= Math::PI * 2.0f;
  
  if( _pitch < Math::degToRad( -89.0f ) )
    _pitch = Math::degToRad( -89.0f );
  else if( _pitch > Math::degToRad( 89.0f ) )
    _pitch = Math::degToRad( 89.0f );
}

void Camera::calcDir()
{
  normalizeYawPitch();

//   _dir = Vector3f( sinf( _yaw ) * cosf( _pitch ),
// 		   -sinf( _pitch ),
// 		   cosf( _yaw ) * cosf( _pitch ) ).normalized();

//   _dir = Vector3f( cosf( _yaw ) * cosf( _pitch ),
// 		   sinf( _yaw ),
// 		   cosf( _yaw ) * sinf( _pitch ) );

//   _dir = Vector3f( cosf( _yaw ),
// 		   0.0f,
// 		   sinf( _yaw ) );

  //_dir = Vector3f(0,0,-1); // FIXME

  Matrix3f matrix = ( AngleAxisf( _yaw, Vector3f::UnitY() ) * AngleAxisf( _pitch, Vector3f::UnitX() ) ).toRotationMatrix();
  _dir = matrix * Vector3f( 0, 0, -1 );

  _right = Vector3f::UnitY().cross( _dir ).normalized() * -1.0f;
  _up  = _dir.cross( _right ).normalized() * -1.0f;

  //printf( "[%.1f %.1f %.1f] [%.1f %.1f %.1f] [%.1f %.1f %.1f]\n", _dir[0], _dir[1], _dir[2], _right[0], _right[1], _right[2], _up[0], _up[1], _up[2] );
}
