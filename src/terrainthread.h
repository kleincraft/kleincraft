#ifndef __TERRAINTHREAD_H__
#define __TERRAINTHREAD_H__

#include <list>

#include <pthread.h>

class AbstractTerrainGenerator;
class TerrainPage;

class TerrainThread
{
public:
  struct Request
  {
    enum Type {
      
      Quit,
      GeneratePage,
    };
    
    int type;
    TerrainPage* page;
    int depth;
  };
  
  TerrainThread( AbstractTerrainGenerator* generator );

  bool start();
  void stop();

  //! Locks the send queue...
  void beginSend();
  //! Unlocks the send queue and signals the wait condition...
  void endSend();

  //! Must be executed in between beginSend and endSend calls!
  void sendRequest( const Request& req );
  //! Must be executed in between beginSend and endSend calls!
  void sendQuit();
  //! Must be executed in between beginSend and endSend calls!
  void sendGeneratePage( TerrainPage* page, int depth );

private:
  static void* callback( void* arg );
  void run();

  pthread_t _thread;

  std::list< Request > _queueIn;
  pthread_mutex_t _queueInMutex;
  pthread_cond_t _queueInCond;

  AbstractTerrainGenerator* _generator;
};

#endif // __TERRAINTHREAD_H__
