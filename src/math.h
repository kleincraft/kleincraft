#ifndef __KCMATH_H__
#define __KCMATH_H__

namespace Math
{
  static const float PI = 3.141592654f;
  static const float DEG_TO_RAD = PI / 180.0f;
  
  static float degToRad( float d )
  {
    return d * DEG_TO_RAD;
  }
  
  static float radToDeg( float r )
  {
    return r / DEG_TO_RAD;
  }
}

#endif // __KCMATH_H__
