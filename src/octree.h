#ifndef __OCTREE_H__
#define __OCTREE_H__

#include "aabox.h"
#include "terrainleaf.h"

#include <Eigen/Core>
using namespace Eigen;

#include <vector>

#include <stdio.h>

class Camera;
class RenderState;

struct Octree
{
  struct Node
  {
    Node( /*Node* parent,*/ unsigned char depth );
    ~Node();

    //! Edge length of node
    int getSize() const
    {
      return 1 << _depth;
    }

    //! Number of cubes in node
    int getNumCubes() const
    {
      int s = getSize();
      return s * s * s;
    }

    //! The coordinates must be in [0..SIZE-1]
    //! \param x Coordinates of leaf, relative to node, x coordinate
    //! \param y Coordinates of leaf, relative to node, y coordinate
    //! \param z Coordinates of leaf, relative to node, z coordinate
    TerrainLeaf* getLeaf( int x, int y, int z );

    bool savePlain( FILE* fp, int count );

    //! \param camera Camera that defines the frustum
    //! \param ox Position of the node in world coordinates, x coordinate
    //! \param oy Position of the node in world coordinates, y coordinate
    //! \param oz Position of the node in world coordinates, z coordinate
    void frustumCull( Camera* camera, int ox, int oy, int oz, int* numCulled );

    //! \param renderState Rendering API for the terrain
    //! \param ox Position of the node in world coordinates, x coordinate
    //! \param oy Position of the node in world coordinates, y coordinate
    //! \param oz Position of the node in world coordinates, z coordinate
    void render( RenderState* renderState, int ox, int oy, int oz  );

    void castRay( const Vector3f& pos, const Vector3f& dir, 
		  int x, int y, int z,
		  TerrainLeaf** leaf, int* lx, int* ly, int* lz, /*int *face,*/ float* d );
    
    void testCollision( const AABox& boxIn, const Vector3f& dpIn, 
			int xIn, int yIn, int zIn,
			std::vector< TerrainLeafCollision >* collisionsOut );

    AABox getAACube(int x, int y, int z) const;
    
    //Node* _parent;

    union {
      
      Node* _children[8]; // Nodes down to depth 2 have nodes as children
      TerrainLeaf _leafs[8]; // Nodes at depth 1 have leafs as children
    };

    unsigned char _depth; // Actually "reverse depth", ie bottom node has depth 1
    bool _culled; // Result of frustum culling
	//char _padding[2];
  };

  Octree( unsigned char depth );
  ~Octree();
  
  //! Coordinates are relative to octree
  TerrainLeaf* getLeaf( int rx, int ry, int rz );

  bool savePlain( const char* filename );
  
  //! \param ox Position of the octree in world coordinates, x coordinate
  //! \param oy Position of the octree in world coordinates, y coordinate
  //! \param oz Position of the octree in world coordinates, z coordinate
  void frustumCull( Camera* camera, int ox, int oy, int oz, int* numCulled );

  //! \param ox Position of the octree in world coordinates, x coordinate
  //! \param oy Position of the octree in world coordinates, y coordinate
  //! \param oz Position of the octree in world coordinates, z coordinate
  void render( RenderState* renderState, int ox, int oy, int oz );

  //! Calculates obstuction flags for all nodes
  //! Expensive!
  void updateObstruction();

  //! Edge length of octree
  int getSize() const { return 1 << _depth; }

  //! Number of cubes in octree
  int getNumCubes() const 
  { 
    int s = getSize();
    return s * s * s;
  }

  void clear();

  void castRay( const Vector3f& pos, const Vector3f& dir, 
		int ox, int oy, int oz,
		TerrainLeaf** leaf, int* lx, int* ly, int* lz, /*int *face,*/ float* d );

  void testCollision( const AABox& boxIn, const Vector3f& dpIn, 
		      int xIn, int yIn, int zIn,
		      std::vector< TerrainLeafCollision >* collisionsOut );
  
  unsigned char _depth; // Depth of the octree
  Node* _root; // Root node
};

#endif // __OCTREE_H__
