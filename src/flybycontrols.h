#ifndef __FLYBYCONTROLS_H__
#define __FLYBYCONTROLS_H__

struct AbstractFlyByControls
{
  virtual void update() = 0;

  virtual bool isQuit() const = 0;

  virtual Vector3f getDPos() const = 0;
  virtual float getDYaw() const = 0;
  virtual float getDPitch() const = 0;

  virtual float getDDepth() const = 0;

  virtual bool getPutBlock() const = 0;
  virtual void resetPutBlock() = 0;

  virtual bool getRemoveBlock() const = 0;
  virtual void resetRemoveBlock() = 0;
};

#endif // __FLYBYCONTROLS_H__
