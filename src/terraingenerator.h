#ifndef __TERRAINGENERATOR_H__
#define __TERRAINGENERATOR_H__

class Octree;

struct AbstractTerrainGenerator
{
  virtual void generate( Octree* octree,
			 int ox, int oy, int oz ) = 0;
};

#endif // __TERRAINGENERATOR_H__
