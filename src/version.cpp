#include "version.h"

#include <stdio.h>

std::string getVersionString()
{
  char buffer[32];
  snprintf( buffer, sizeof(buffer), "V%d.%d.%d", VERSION_MAJOR, VERSION_MINOR, VERSION_BUILD );
  return std::string(buffer);
}
