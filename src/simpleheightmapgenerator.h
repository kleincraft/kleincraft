#ifndef __SIMPLEHEIGHTMAPGENERATOR_H__
#define __SIMPLEHEIGHTMAPGENERATOR_H__

#include "terraingenerator.h"

struct SimpleHeightMapGenerator : public AbstractTerrainGenerator
{
  SimpleHeightMapGenerator( int totalH );
  
  void generate( Octree* octree, int ox, int oy, int oz );

  int totalH;
};

#endif // __SIMPLEHEIGHTMAPGENERATOR_H__
