#include "renderer.h"
#if USE_RENDERER == RENDERER_GLES 

#include "gles11.h"

#include <egl.h>

namespace GLES11
{

static EGLDisplay _display = 0;
static EGLSurface _surface = 0;
static EGLContext _context = 0;
static NativeWindowType* _window = 0;

static NativeWindowType* allocWindow()
{
  return static_cast< NativeWindowType*>( malloc( 16 * 1024 ) );
}

bool initGLES()
{
  _window = allocWindow();
  if( !_window ) {

    fprintf( stderr, "allocWindow\n" );
    return false;
  }

  _display = eglGetDisplay( EGL_DEFAULT_DISPLAY );

  EGLint major;
  EGLint minor;

  if( !eglInitialize( _display, &major, &minor ) ) {

    fprintf( stderr, "eglInitialize\n" );
    return false;
  }

  const EGLint config[] = {
    
    EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
    EGL_NONE, EGL_NONE
  };
  const EGLint MaxChoices = 10;
  EGLConfig choices[ MaxChoices ];
  EGLint numChoices = 0;
  eglChooseConfig( _display, config, choices, MaxChoices, &numChoices );

  if( numChoices < 1 ) {

    fprintf( stderr, "eglChooseConfig\n" );
    return false;
  }

  _surface = eglCreateWindowSurface( _display, choices[ 0 ], _window, config );
  if( !_surface ) {
    
    fprintf( stderr, "eglCreateWindowSurface\n" );
    return false;
  }

  _context = eglCreateContext( _display, choices[ 0 ], 0, config );
  if( !_context ) {
    
    fprintf( stderr, "eglCreateContext\n" );
    return false;
  }

  eglMakeCurrent( _display, _surface, _surface, _context );

  return true;
}

void shutdownGLES()
{
  if( _display ) {
    
    eglMakeCurrent( _display, 0, 0, 0 );
    
    if( _context ) 
      eglDestroyContext( _display, _context );
    
    if( _surface )
      eglDestroySurface( _display, _surface );

    eglTerminate( _display );
  }
}

void swapBuffers()
{
  eglWaitGL();
  eglSwapBuffers( _display, _surface );
}

} // namespace 

#endif // RENDERER_GLES
