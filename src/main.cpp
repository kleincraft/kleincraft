#include "aabox.h"
#include "camera.h"
#include "flybycontrols.h"
#include "font.h"
#include "fpscontrols.h"
#include "octree.h"
#include "options.h"
#include "renderer.h"
#include "simpleheightmapgenerator.h"
#include "terrain.h"
#include "texture.h"
#include "version.h"

#include <SDL.h>

#include <gl.h>

#include <algorithm>
#include <cassert>

#include <stdlib.h>

// struct InputState
// {
//   InputState()
//   {
//     reset();
//   }
//
//   void reset()
//   {
//     up = down = left = right = a = b = c = d = x = y = plus = minus = putBlock = removeBlock = quit = false;
//   }
//
//   bool up;
//   bool down;
//   bool left;
//   bool right;
//   bool a;
//   bool b;
//   bool c;
//   bool d;
//   bool x;
//   bool y;
//
//   bool plus;
//   bool minus;
//
//   bool putBlock;
//   bool removeBlock;
//
//   bool quit;
// };


#define PLATFORM_PC 1
#define PLATFORM_WIZ 2
#define PLATFORM_CAANOO 3

#if USE_PLATFORM == PLATFORM_WIZ
enum WizButtons
{
  WizUpButton = 0x00,
  WizUpLeftButton,
  WizLeftButton,
  WizDownLeftButton,
  WizDownButton,
  WizDownRightButton,
  WizRightButton,
  WizUpRightButton,
  WizMenuButton,
  WizSelectButton,
  WizLeftShoulderButton,
  WizRightShoulderButton,
  WizAButton,
  WizBButton,
  WizXButton,
  WizYButton,
  WizVolPlusButton,
  WizVolMinusButton
};

static bool platform_initInput()
{
  if( SDL_Init( SDL_INIT_JOYSTICK ) < 0 ) {
    
    fprintf( stderr, "ERROR: %s\n", SDL_GetError() );
    return false;
  }
  
  SDL_JoystickEventState( SDL_ENABLE );
  return SDL_JoystickOpen( 0 ) != 0;
}

struct WizFlyByControls : public AbstractFlyByControls
{
  WizFlyByControls()
    : _quit( false ), _buttons( 0 )
  {
  }

  void update()
  {
    SDL_Event event;
    while( SDL_PollEvent( &event ) ) {

      switch( event.type ) {

      case SDL_QUIT:
	_quit = true;
	break;

      case SDL_JOYBUTTONDOWN:
	_buttons |= ( 1 << event.button.button );
	break;
      
      case SDL_JOYBUTTONUP:
	_buttons &= ~( 1 << event.button.button );
	break;
      }
    }
  }

  bool isQuit() const
  {
    return _quit || ( isButton( WizMenuButton ) && isButton( WizSelectButton ) );
  }

  bool isButton( unsigned button ) const
  {
    return _buttons & ( 1 << button );
  }

  bool isUp() const
  {
    return isButton( WizUpButton ) || isButton( WizUpLeftButton ) || isButton( WizUpRightButton );
  }
  
  bool isDown() const
  {
    return isButton( WizDownButton ) || isButton( WizDownLeftButton ) || isButton( WizDownRightButton );
  }
  
  bool isLeft() const
  {
    return isButton( WizLeftButton ) || isButton( WizUpLeftButton ) || isButton( WizDownLeftButton );
  }

  bool isRight() const
  {
    return isButton( WizRightButton ) || isButton( WizUpRightButton ) || isButton( WizDownRightButton );
  }

  bool isAlt() const
  {
    return isButton( WizRightShoulderButton ) || isButton( WizSelectButton );
  }

  Vector3f getDPos() const
  {
    const float DX = 0.1f;
    const float DY = 0.1f;
    const float DZ = 0.1f;

    float dx = 0;
    float dy = 0;
    float dz = 0;

    if( isAlt() ) {
      
      if( isLeft() )
	dx -= DX;
      if( isRight() )
	dx += DX;
      
//       if( isUp() )
// 	dy += DY;
//       if( isDown() )
// 	dy -= DY;
    }
    else {

      if( isUp() )
	dz += DZ;
      if( isDown() )
	dz -= DZ;
    }

    return Vector3f(dx,dy,dz);
  }

  float getDYaw() const
  {
    const float DYAW = 2.0f;
    float dYaw = 0;

    if( !isAlt() ) {

      if( isLeft() )
	dYaw += DYAW;
      if( isRight() )
	dYaw -= DYAW;
    }

    return dYaw;
  }
  
  float getDPitch() const
  {
    const float DPITCH = 2.0f;
    float dPitch = 0;

    if( isAlt() ) {

      if( isUp() )
	dPitch -= DPITCH;
      if( isDown() )
	dPitch += DPITCH;
    }

    return dPitch;
  }

  float getDDepth() const
  {
    return 0.0f;
  }

  bool getPutBlock() const
  {
    return isButton( WizAButton );
  }

  void resetPutBlock()
  {
    _buttons &= ~( 1 << WizAButton );
  }

  bool getRemoveBlock() const
  {
    return isButton( WizXButton );
  }
  
  void resetRemoveBlock()
  {
    _buttons &= ~( 1 << WizXButton );
  }
  
  bool _quit;
  unsigned _buttons;
};

// enum {
//
//   LeftCount,
//   RightCount,
//   DownCount,
//   UpCount
// };
//
// static int wiz_dirCount[4] = {0,0,0,0};
//
// static void platform_updateInput( InputState* input )
// {
//   SDL_Event e;
//   while( SDL_PollEvent( &e ) ) {
//    
//     switch( e.type ) {
//      
//     case SDL_QUIT:
//       input->quit = true;
//       break;
//
//     case SDL_JOYBUTTONDOWN:
//       switch( e.button.button ) {
//	
//       case WizMenuButton:
// 	input->quit = true;
// 	break;
//
//       case WizLeftButton:
// 	wiz_dirCount[LeftCount]++;
// 	break;
//       case WizUpLeftButton:
// 	wiz_dirCount[UpCount]++;
// 	wiz_dirCount[LeftCount]++;
// 	break;
//       case WizDownLeftButton:
// 	wiz_dirCount[DownCount]++;
// 	wiz_dirCount[LeftCount]++;
// 	break;
//       case WizRightButton:
// 	wiz_dirCount[RightCount]++;
// 	break;
//       case WizUpRightButton:
// 	wiz_dirCount[UpCount]++;
// 	wiz_dirCount[RightCount]++;
// 	break;
//       case WizDownRightButton:
// 	wiz_dirCount[DownCount]++;
// 	wiz_dirCount[RightCount]++;
// 	break;
//       case WizUpButton:
// 	wiz_dirCount[UpCount]++;
// 	break;
//       case WizDownButton:
// 	wiz_dirCount[DownCount]++;
// 	break;
//
//       case WizYButton:
// 	input->a = true;
// 	break;
//       case WizAButton:
// 	input->b = true;
// 	break;
//       case WizXButton:
// 	input->c = true;
// 	break;
//       case WizBButton:
// 	input->d = true;
// 	break;
//       case WizLeftShoulderButton:
// 	input->x = true;
// 	break;
//       case WizRightShoulderButton:
// 	input->y = true;
// 	break;
//       }
//       break;
//
// //     case SDL_KEYDOWN:
// //       switch( e.key.keysym.sym ) {
// //
// //       case SDLK_w:
// // 	input->up = true;
// // 	break;
// //       case SDLK_a:
// // 	input->left = true;
// // 	break;
// //       case SDLK_s:
// // 	input->down = true;
// // 	break;
// //       case SDLK_d:
// // 	input->right = true;
// // 	break;
// //
// //       case SDLK_UP:
// // 	input->a = true;
// // 	break;
// //       case SDLK_LEFT:
// // 	input->b = true;
// // 	break;
// //       case SDLK_DOWN:
// // 	input->c = true;
// // 	break;
// //       case SDLK_RIGHT:
// // 	input->d = true;
// // 	break;
// //
// //       case SDLK_ESCAPE:
// // 	input->quit = true;
// // 	break;
// //
// //       default:
// // 	break;
// //       }
// //       break;
// //
// //     case SDL_KEYUP:
// //       switch( e.key.keysym.sym ) {
// //
// //       case SDLK_w:
// // 	input->up = false;
// // 	break;
// //       case SDLK_a:
// // 	input->left = false;
// // 	break;
// //       case SDLK_s:
// // 	input->down = false;
// // 	break;
// //       case SDLK_d:
// // 	input->right = false;
// // 	break;
// //
// //       case SDLK_UP:
// // 	input->a = false;
// // 	break;
// //       case SDLK_LEFT:
// // 	input->b = false;
// // 	break;
// //       case SDLK_DOWN:
// // 	input->c = false;
// // 	break;
// //       case SDLK_RIGHT:
// // 	input->d = false;
// // 	break;
// //
// //       default:
// // 	break;
// //       }
// //       break;
//      
//     case SDL_JOYBUTTONUP:
//       switch( e.button.button ) {
//
//       case WizLeftButton:
// 	wiz_dirCount[LeftCount]--;
// 	break;
//       case WizUpLeftButton:
// 	wiz_dirCount[UpCount]--;
// 	wiz_dirCount[LeftCount]--;
// 	break;
//       case WizDownLeftButton:
// 	wiz_dirCount[DownCount]--;
// 	wiz_dirCount[LeftCount]--;
// 	break;
//       case WizRightButton:
// 	wiz_dirCount[RightCount]--;
// 	break;
//       case WizUpRightButton:
// 	wiz_dirCount[UpCount]--;
// 	wiz_dirCount[RightCount]--;
// 	break;
//       case WizDownRightButton:
// 	wiz_dirCount[DownCount]--;
// 	wiz_dirCount[RightCount]--;
// 	break;
//       case WizUpButton:
// 	wiz_dirCount[UpCount]--;
// 	break;
//       case WizDownButton:
// 	wiz_dirCount[DownCount]--;
// 	break;
//
//       case WizYButton:
// 	input->a = false;
// 	break;
//       case WizAButton:
// 	input->b = false;
// 	break;
//       case WizXButton:
// 	input->c = false;
// 	break;
//       case WizBButton:
// 	input->d = false;
// 	break;
//       case WizLeftShoulderButton:
// 	input->x = false;
// 	break;
//       case WizRightShoulderButton:
// 	input->y = false;
// 	break;
//       }
//       break;
//     }
//     break;
//   }
//
//   input->left = wiz_dirCount[LeftCount] > 0;
//   input->right = wiz_dirCount[RightCount] > 0;
//   input->down = wiz_dirCount[DownCount] > 0;
//   input->up = wiz_dirCount[UpCount] > 0;
// }

#elif USE_PLATFORM == PLATFORM_PC

static bool platform_initInput()
{
  return true;
}

struct PCFlyByControls : public AbstractFlyByControls
{
  bool _quit;
  Uint8* _keyState;

  PCFlyByControls()
    : _quit(false), _keyState(0)
  {
  }

  void update()
  {
    SDL_Event event;
    while( SDL_PollEvent( &event ) ) {

      switch( event.type ) {

      case SDL_QUIT:
	_quit = true;
	break;
      }
    }

    int dummy;
    _keyState = SDL_GetKeyState( &dummy );
  }
  
  bool isKey( unsigned key ) const
  {
    return _keyState[ key ];
  }

  bool isQuit() const
  {
    return _quit || isKey( SDLK_ESCAPE );
  }

  Vector3f getDPos() const
  {
    const float DX = 0.1f;
    const float DY = 0.1f;
    const float DZ = 0.1f;

    float dx = 0;
    float dy = 0;
    float dz = 0;

    if( isKey( SDLK_LALT ) || isKey( SDLK_RALT ) ) {
      
      if( isKey( SDLK_LEFT ) )
	dx -= DX;
      if( isKey( SDLK_RIGHT ) )
	dx += DX;
      
      if( isKey( SDLK_UP ) )
	dy += DY;
      if( isKey( SDLK_DOWN ) )
	dy -= DY;
    }
    else {

      if( isKey( SDLK_UP ) )
	dz += DZ;
      if( isKey( SDLK_DOWN ) )
	dz -= DZ;
    }

    return Vector3f(dx,dy,dz);
  }

  float getDYaw() const
  {
    const float DYAW = 2.0f;
    float dYaw = 0;

    if( !( isKey( SDLK_LALT ) || isKey( SDLK_RALT ) ) ) {

      if( isKey( SDLK_LEFT ) )
	dYaw += DYAW;
      if( isKey( SDLK_RIGHT ) )
	dYaw -= DYAW;
    }

    return dYaw;
  }

  float getDPitch() const
  {
    const float DPITCH = 2.0f;
    float dPitch = 0;

    if( isKey( SDLK_PAGEUP ) )
      dPitch -= DPITCH;
    if( isKey( SDLK_PAGEDOWN ) )
      dPitch += DPITCH;

    return dPitch;
  }

  float getDDepth() const
  {
    const float DDEPTH = 8.0f;
    float dDepth = 0.0f;

    if( isKey( SDLK_MINUS ) || isKey( SDLK_KP_MINUS ) )
      dDepth -= DDEPTH;
    if( isKey( SDLK_PLUS ) || isKey( SDLK_KP_PLUS ) )
      dDepth += DDEPTH;

    return dDepth;
  }

  bool getPutBlock() const
  {
    return isKey( SDLK_SPACE );
  }

  void resetPutBlock()
  {
    _keyState[ SDLK_SPACE ] = 0;
  }

  bool getRemoveBlock() const
  {
    return isKey( SDLK_x );
  }

  void resetRemoveBlock()
  {
    _keyState[ SDLK_x ] = 0;
  }
};

struct PCFpsControls : public AbstractFpsControls
{
  bool _quit;
  Uint8* _keyState;

  PCFpsControls()
    : _quit(false), _keyState(0)
  {
  }

  void update()
  {
    SDL_Event event;
    while( SDL_PollEvent( &event ) ) {

      switch( event.type ) {

      case SDL_QUIT:
	_quit = true;
	break;
      }
    }

    int dummy;
    _keyState = SDL_GetKeyState( &dummy );
  }
  
  bool isKey( unsigned key ) const
  {
    return _keyState[ key ];
  }

  bool isQuit() const
  {
    return _quit || isKey( SDLK_ESCAPE );
  }

  Vector3f getDPos() const
  {
    const float DX = 0.1f;
    const float DY = 0.1f;
    const float DZ = 0.1f;

    float dx = 0;
    float dy = 0;
    float dz = 0;

    if( isKey( SDLK_LALT ) || isKey( SDLK_RALT ) ) {
      
      if( isKey( SDLK_LEFT ) )
	dx -= DX;
      if( isKey( SDLK_RIGHT ) )
	dx += DX;
      
      if( isKey( SDLK_UP ) )
	dy += DY;
      if( isKey( SDLK_DOWN ) )
	dy -= DY;
    }
    else {

      if( isKey( SDLK_UP ) )
	dz += DZ;
      if( isKey( SDLK_DOWN ) )
	dz -= DZ;
    }

    return Vector3f(dx,dy,dz);
  }

  float getDYaw() const
  {
    const float DYAW = 2.0f;
    float dYaw = 0;

    if( !( isKey( SDLK_LALT ) || isKey( SDLK_RALT ) ) ) {

      if( isKey( SDLK_LEFT ) )
	dYaw += DYAW;
      if( isKey( SDLK_RIGHT ) )
	dYaw -= DYAW;
    }

    return dYaw;
  }

  float getDPitch() const
  {
    const float DPITCH = 2.0f;
    float dPitch = 0;

    if( isKey( SDLK_PAGEUP ) )
      dPitch -= DPITCH;
    if( isKey( SDLK_PAGEDOWN ) )
      dPitch += DPITCH;

    return dPitch;
  }

  float getDDepth() const
  {
    const float DDEPTH = 8.0f;
    float dDepth = 0.0f;

    if( isKey( SDLK_MINUS ) || isKey( SDLK_KP_MINUS ) )
      dDepth -= DDEPTH;
    if( isKey( SDLK_PLUS ) || isKey( SDLK_KP_PLUS ) )
      dDepth += DDEPTH;

    return dDepth;
  }

  bool getPutBlock() const
  {
    return isKey( SDLK_SPACE );
  }

  void resetPutBlock()
  {
    _keyState[ SDLK_SPACE ] = 0;
  }

  bool getRemoveBlock() const
  {
    return isKey( SDLK_x );
  }

  void resetRemoveBlock()
  {
    _keyState[ SDLK_x ] = 0;
  }
};

// static void platform_updateInput( InputState* input )
// {
//   input->plus = false;
//   input->minus = false;
//
//   SDL_Event e;
//   while( SDL_PollEvent( &e ) ) {
//
//     switch( e.type ) {
//
//     case SDL_QUIT:
//       input->quit = true;
//       break;
//
//     case SDL_KEYDOWN:
//       switch( e.key.keysym.sym ) {
//
//       case SDLK_w:
// 	input->up = true;
// 	break;
//       case SDLK_a:
// 	input->left = true;
// 	break;
//       case SDLK_s:
// 	input->down = true;
// 	break;
//       case SDLK_d:
// 	input->right = true;
// 	break;
//
//       case SDLK_UP:
// 	input->a = true;
// 	break;
//       case SDLK_LEFT:
// 	input->b = true;
// 	break;
//       case SDLK_DOWN:
// 	input->c = true;
// 	break;
//       case SDLK_RIGHT:
// 	input->d = true;
// 	break;
//
//       case SDLK_n:
// 	input->x = true;
// 	break;
//       case SDLK_m:
// 	input->y = true;
// 	break;
//
//       case SDLK_PLUS:
//       case SDLK_KP_PLUS:
//  	input->plus = true;
// 	break;
//
//       case SDLK_MINUS:
//       case SDLK_KP_MINUS:
// 	input->minus = true;
// 	break;
//
//       case SDLK_x:
// 	input->removeBlock = true;
// 	break;
//
//       case SDLK_SPACE:
// 	input->putBlock = true;
// 	break;
//
//       case SDLK_ESCAPE:
// 	input->quit = true;
// 	break;
//
//       default:
// 	break;
//       }
//       break;
//
//     case SDL_KEYUP:
//       switch( e.key.keysym.sym ) {
//
//       case SDLK_w:
// 	input->up = false;
// 	break;
//       case SDLK_a:
// 	input->left = false;
// 	break;
//       case SDLK_s:
// 	input->down = false;
// 	break;
//       case SDLK_d:
// 	input->right = false;
// 	break;
//
//       case SDLK_UP:
// 	input->a = false;
// 	break;
//       case SDLK_LEFT:
// 	input->b = false;
// 	break;
//       case SDLK_DOWN:
// 	input->c = false;
// 	break;
//       case SDLK_RIGHT:
// 	input->d = false;
// 	break;
//
//       case SDLK_n:
// 	input->x = false;
// 	break;
//       case SDLK_m:
// 	input->y = false;
// 	break;
//
//       default:
// 	break;
//       }
//       break;
//     }
//     break;
//   }
// }

#elif USE_PLATFORM == PLATFORM_CAANOO

enum CaanooButtons
{
  CaanooAButton = 0,
  CaanooXButton,
  CaanooBButton,
  CaanooYButton,
  CaanooLButton,
  CaanooRButton,
  CaanooHomeButton,
  CaanooHoldButton,
  CaanooHelp1Button,
  CaanooHelp2Button,
  CaanooTactButton,
};

SDL_Joystick* _joystick(0);

static bool platform_initInput()
{
  if( SDL_Init( SDL_INIT_JOYSTICK ) < 0 ) {
    
    fprintf( stderr, "ERROR: %s\n", SDL_GetError() );
    return false;
  }
  
  SDL_JoystickEventState( SDL_ENABLE );
  return (_joystick = SDL_JoystickOpen( 0 )) != 0;
}

struct CaanooFlyByControls : public AbstractFlyByControls
{
  CaanooFlyByControls()
    : _quit( false ), _buttons( 0 )
  {
  }

  void update()
  {
    SDL_Event event;
    while( SDL_PollEvent( &event ) ) {

      switch( event.type ) {

      case SDL_QUIT:
	_quit = true;
	break;

      case SDL_JOYBUTTONDOWN:
	_buttons |= ( 1 << event.button.button );
	break;
      
      case SDL_JOYBUTTONUP:
	_buttons &= ~( 1 << event.button.button );
	break;
      }
    }
  }

  bool isQuit() const
  {
    return _quit || ( isButton( CaanooHelp1Button ) && isButton( CaanooHelp2Button ) );
  }

  bool isButton( unsigned button ) const
  {
    return _buttons & ( 1 << button );
  }

  bool isUp() const
  {
    return SDL_JoystickGetAxis(_joystick, 0) <= -2000;
  }
  
  bool isDown() const
  {
    return SDL_JoystickGetAxis(_joystick, 0) >= 2000;
  }
  
  bool isLeft() const
  {
    return SDL_JoystickGetAxis(_joystick, 1) <= -2000;
  }

  bool isRight() const
  {
    return SDL_JoystickGetAxis(_joystick, 1) >= 2000;
  }

  bool isAlt() const
  {
    return isButton( CaanooRButton ) || isButton( CaanooHomeButton );
  }

  Vector3f getDPos() const
  {
    const float DX = 0.1f;
    const float DY = 0.1f;
    const float DZ = 0.1f;

    float dx = 0;
    float dy = 0;
    float dz = 0;

    if( isAlt() ) {
      
      if( isLeft() )
	dx -= DX;
      if( isRight() )
	dx += DX;
      
//       if( isUp() )
// 	dy += DY;
//       if( isDown() )
// 	dy -= DY;
    }
    else {

      if( isUp() )
	dz += DZ;
      if( isDown() )
	dz -= DZ;
    }

    return Vector3f(dx,dy,dz);
  }

  float getDYaw() const
  {
    const float DYAW = 2.0f;
    float dYaw = 0;

    if( !isAlt() ) {

      if( isLeft() )
	dYaw += DYAW;
      if( isRight() )
	dYaw -= DYAW;
    }

    return dYaw;
  }
  
  float getDPitch() const
  {
    const float DPITCH = 2.0f;
    float dPitch = 0;

    if( isAlt() ) {

      if( isUp() )
	dPitch -= DPITCH;
      if( isDown() )
	dPitch += DPITCH;
    }

    return dPitch;
  }

  float getDDepth() const
  {
    return 0.0f;
  }

  bool getPutBlock() const
  {
    return isButton( CaanooAButton );
  }

  void resetPutBlock()
  {
    _buttons &= ~( 1 << CaanooAButton );
  }

  bool getRemoveBlock() const
  {
    return isButton( CaanooXButton );
  }
  
  void resetRemoveBlock()
  {
    _buttons &= ~( 1 << CaanooXButton );
  }
  
  bool _quit;
  unsigned _buttons;
};

#else
#error Unknown platform
#endif

static void renderLoadingScreen( Renderer* renderer, Font* font )
{
    renderer->startFrame();
    glClear( GL_COLOR_BUFFER_BIT );

    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();

    //gluOrtho2D( 0, 320, 0, 240 );
#if USE_RENDERER == RENDERER_GLES
    glOrthof( 0, 320, 0, 240, -1, 1 );
#else
    glOrtho( 0, 320, 0, 240, -1, 1 );
#endif

    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();

    font->printf( renderer, Vector2f( 90, 120 ), "KLEINCRAFT %s", getVersionString().c_str() );
    font->printf( renderer, Vector2f( 90, 110 ), "INITIALIZING..." );

    renderer->renderFrame();
}

static void setFarD( Camera* camera, float farD ) 
{
  //glFogf( GL_FOG_START, farD - (farD * 0.9f) );
  //glFogf( GL_FOG_END, farD );
  
  camera->setupFrustum( 45, 320.0/240.0, 1, farD );
}

/*static*/ void renderLoop( Renderer* renderer, /*Octree* octree,*/ AbstractTerrain* terrain, Font* font, Texture* texTerrain,
							AbstractFlyByControls* controls,
							const Options& options )
{
  //glFogi( GL_FOG_MODE, GL_LINEAR );

//  Vector3f cameraPos0( terrain->getSizeX() / 2, terrain->getSizeY(), terrain->getSizeZ() / 2 );
//  {
//    Octree* octree(0);
//    TerrainLeaf* leaf(0);
//    int lx;
//    int ly;
//    int lz;
//    float d;
//    
//    terrain->castRay( cameraPos0, Vector3f(0,-1,0),
//		      &octree, &leaf, &lx, &ly, &lz, &d );
//
//    cameraPos0 = cameraPos0 + Vector3f(0,-1,0) * d + Vector3f(0,5,0);
//  }

  Camera camera;
  camera.set( Vector3f( options.posx, options.posy, options.posz ), options.yaw, options.pitch );

  float farD = options.far;
  setFarD( &camera, farD );

  //   {
  //      unsigned numCulled;
  //      Uint32 ticks0 = SDL_GetTicks();
  //      printf( "culling...\n" );
  //      octree->frustumCull( &camera, &numCulled );
  //      printf( "%u ms\n", SDL_GetTicks() - ticks0 );
  //      printf( "numCulled = %u\n", numCulled );
  //   }

  const Uint32 FPS = 30;
  const Uint32 MinFPS = 5;
  const Uint32 Period = 1000 / FPS;
  const Uint32 MaxPeriod = 1000 / MinFPS;

  Uint32 ticks0 = SDL_GetTicks();
  Uint32 ticks = 0;

  float fps = 0;
  float fpsRender = 0;
  float fpsCull = 0;
  Uint32 frameCount = 0;
  Uint32 cullTicks = 0; // Time it took to cull...
  Uint32 renderTicks = 0; // Time it took to render...
  Uint32 numTrisRendered = 0;

  //InputState input;

  TerrainRenderState terrainRenderState( renderer, texTerrain );
  for( ;; ) {

    // Sync...

    Uint32 ticks1 = SDL_GetTicks();
    Uint32 dTicks = ticks1 - ticks0;
//    if( dTicks < Period ) {
//
//      SDL_Delay( Period - dTicks );
//      ticks1 = SDL_GetTicks();
//      dTicks = ticks1 - ticks0;
//    }
    ticks0 = ticks1;

    float dt = (float) std::min( dTicks, MaxPeriod ) / (float) Period;

    // Input...

    controls->update();
    
    if( controls->isQuit() )
      break;

    float dDepth = controls->getDDepth() * dt;
    if( dDepth != 0.0f ) {

      farD += dDepth;
      farD = std::max( 32.0f, farD );
      setFarD( &camera, farD );
    }

    AABox boxOld( camera.getPos() + Vector3f(-0.4, -0.4, -0.4), 0.8 );

    Vector3f dPos = controls->getDPos() * dt;
    float dYaw = controls->getDYaw() * dt;
    float dPitch = controls->getDPitch() * dt;
    camera.move( dPos[0], dPos[1], dPos[2], dYaw, dPitch );

    AABox boxNew( camera.getPos() + Vector3f(-0.4, -0.4, -0.4), 0.8 );

    Vector3f dp = boxNew.vMin - boxOld.vMin;

    //AABox boxNew(boxTmp);
    //terrain->sweepAABox( boxOld, dp, &boxNew );

    std::vector< TerrainLeafCollision > collisions;
    terrain->testCollision( boxOld, dp, &collisions );
    if( collisions.size() ){
      
      std::sort( collisions.begin(), collisions.end(), TerrainLeafCollisionCmp() );

//       int count = 0;
//       for( unsigned i = 0; i < collisions.size(); ++i ) {
//
// 	count++;
// 	std::cerr << "(" << collisions[i].x << "," << collisions[i].y << "," << collisions[i].z << ") ";
//       }
//      
//       if( count ) {
//
// 	std::cerr << std::endl;
// 	std::cerr << count << " collisions" << std::endl;
//       }

      //dp *= collisions[0].t0;
      dp = Vector3f(0,0,0);
      boxNew = AABox( boxOld.vMin + dp, 0.8 );
    }

    // Constrain to terrain borders...
    {
      //AABox box( camera.getPos() - Vector3f(-0.4, -0.4, -0.4), 0.8 );
      //AABox box(boxNew);
      //AABox box(boxTmp);

      float dx = 0;
      float dy = 0;
      float dz = 0;

      if( boxNew.vMax[0] > terrain->getSizeX() ) {
	
	dx = terrain->getSizeX() - boxNew.vMax[0];
      }
      else if( boxNew.vMin[0] < 0.0f ) {
	
	dx = -boxNew.vMin[0];
      }

      if( boxNew.vMax[1] > terrain->getSizeY() ) {

	dy = terrain->getSizeY() - boxNew.vMax[1];
      }
      else if( boxNew.vMin[1] < 0.0f ) {
	
	dy = -boxNew.vMin[1];
      }

      if( boxNew.vMax[2] > terrain->getSizeZ() ) {

	dz = terrain->getSizeZ() - boxNew.vMax[2];
      }
      else if( boxNew.vMin[2] < 0.0f ) {
	
	dz = -boxNew.vMin[2];
      }

      //camera.setPos( camera.getPos() + Vector3f(dx,dy,dz) );
      camera.setPos( boxNew.vMin + Vector3f(0.4, 0.4, 0.4) + Vector3f(dx,dy,dz) );
    }

    terrain->update( camera.getPos() );
    
    Octree* octree = 0;
    TerrainLeaf* leaf = 0;
    int leaf_x;
    int leaf_y;
    int leaf_z;
    //int leaf_face;
    float leaf_d;
    
    terrain->castRay( camera.getPos(), camera.getDir(),
		      &octree, &leaf, &leaf_x, &leaf_y, &leaf_z, /*&leaf_face,*/ &leaf_d );

    if( /*input.removeBlock*/ controls->getRemoveBlock() ) {

      //input.removeBlock = false;
      controls->resetRemoveBlock();
      if( leaf ) {

	leaf->setBlockType( octree, leaf_x, leaf_y, leaf_z, 0 );
      }
    }

    if( /*input.putBlock*/ controls->getPutBlock() ) {

      //input.putBlock = false;
      controls->resetPutBlock();
      if( leaf ) {

	AABox cube( Vector3f(leaf_x, leaf_y, leaf_z), 1.0f );
	int face = cube.getNearestFace( camera.getPos() + (camera.getDir() * leaf_d) );

	int nextLeaf_x = leaf_x;
	int nextLeaf_y = leaf_y;
	int nextLeaf_z = leaf_z;

	switch( face ) {

	case AABox::FaceLeft:
	  nextLeaf_x = leaf_x - 1;
	  break;
	case AABox::FaceRight:
	  nextLeaf_x = leaf_x + 1;
	  break;
	case AABox::FaceFront:
	  nextLeaf_z = leaf_z + 1;
	  break;
	case AABox::FaceBack:
	  nextLeaf_z = leaf_z - 1;
	  break;
	case AABox::FaceTop:
	  nextLeaf_y = leaf_y + 1;
	  break;
	case AABox::FaceBottom:
	  nextLeaf_y = leaf_y - 1;
	  break;
	default:
	  nextLeaf_x = -1;
	  break;
	}
	
	TerrainLeaf* nextLeaf = octree->getLeaf( nextLeaf_x, nextLeaf_y, nextLeaf_z );
	if( nextLeaf && !nextLeaf->blockType ) {

	  nextLeaf->setBlockType( octree, nextLeaf_x, nextLeaf_y, nextLeaf_z, 1 );
	}
      }
    }

    //terrain->updatePageStates();
    //terrain->sortPages();
    //terrain->requestNewPages( camera.getPos() );

    const Uint32 cullTicks0 = SDL_GetTicks();
	//int numCulled = 0;
    //octree->frustumCull( &camera, &numCulled );
    terrain->frustumCull( &camera );
    cullTicks += SDL_GetTicks() - cullTicks0;

    // Rendering...

    const Uint32 renderTicks0 = SDL_GetTicks();
    renderer->startFrame();

    glLoadIdentity();
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    camera.applyFrustum();
    camera.applyCamera();

    //glEnable( GL_FOG );
    //octree->render( &terrainRenderState );
    terrain->render( &terrainRenderState );
    //glDisable( GL_FOG );

    // Render leaf
    if( leaf ) {
      
      const float d = 0.01f;
      renderer->renderColoredCube( leaf_x - d, leaf_y - d, leaf_z - d, 1.0f + 2.0f * d,
				   1, 0, 0, 1 );
    }

    //renderer->renderColoredCube( -5, 0, 0,1,
    //				 0, 0, 1, 1 );

    // HUD...

    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();

    //gluOrtho2D( 0, 320, 0, 240 );
#if USE_RENDERER == RENDERER_GLES
    glOrthof( 0, 320, 0, 240, -1, 1 );
#else
    glOrtho( 0, 320, 0, 240, -1, 1 );
#endif

    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();

    int y = 240 - 4;

    font->printf( renderer, Vector2f( 4, y ), "YAW: %.1f", camera.getYaw() );
    y -= 10;
    font->printf( renderer, Vector2f( 4, y ), "PIT: %.1f", camera.getPitch() );
    y -= 10;

    Vector3f pos = camera.getPos();
    font->printf( renderer, Vector2f( 4, y ), "POS: %.1f/%.1f/%.1f", pos[0], pos[1], pos[2] );
    y -= 10;
    font->printf( renderer, Vector2f( 4, y ), "FPS: %.1f/%.1f/%.1f", fps, fpsRender, fpsCull );
    y -= 10;
    font->printf( renderer, Vector2f( 4, y ), "TRI: %u", numTrisRendered );
    y -= 10;
    //font->printf( renderer, Vector2f( 4, y ), "CUL: %u/%d", numCulled, octree->getNumCubes() );
    //y -= 10;
    //font->printf( renderer, Vector2f( 4, y ), "NCT: %u", (octree->getNumCubes()-numCulled) * 12 );
    //y -= 10;
    font->printf( renderer, Vector2f( 4, y ), "FAR: %d", (int)farD);
    y -= 10;
    
    if( collisions.size() ) {
      font->printf( renderer, Vector2f( 4, y ), "COL: %u, [%f]", collisions.size(), collisions[0].t0 /*, collisions[0].t1*/ );
    }
    else {
      font->printf( renderer, Vector2f( 4, y ), "COL: --" );
    }
    y -= 10;

    if( leaf ) {

      Vector3f p = camera.getPos() + ( camera.getDir() * leaf_d );

      AABox cube( Vector3f(leaf_x, leaf_y, leaf_z), 1.0f );
      int face = cube.getNearestFace( p );
      
      font->printf( renderer, Vector2f( 4, y ), "LEA: %d/%d/%d %s", leaf_x, leaf_y, leaf_z, AABox::faceToString(face) );
    }
    else  {

      font->printf( renderer, Vector2f( 4, y ), "LEA: --/--/--" );
    }
    y -= 10;

    renderer->renderFrame();
    renderTicks += SDL_GetTicks() - renderTicks0;
    frameCount++;

    numTrisRendered = renderer->getNumTrisRendered();

    // Framerate sync...

    // Stats...

    ticks += dTicks;
    if( ticks >= 1000 ) {

      fps = (1000 * frameCount) / ( renderTicks + cullTicks );
      fpsRender = 100 * renderTicks / ticks;
      fpsCull = 100 * cullTicks / ticks;

      //       printf( "%u tris/s\n", (1000 * numRendered) / ticks );
      //      
      //       if( !frameTicks )
      // 	frameTicks = 1000;
      //
      //       printf( "%u fps\n", 

      //printf( "yaw: %f\n", camera.getYaw() );
      //printf( "pitch: %f\n", camera.getPitch() );

      frameCount = 0;
      cullTicks = 0;
      renderTicks = 0;
      ticks = 0;
    }
  }
}

#define FLOATARG(var) if(!nextArg.size()) { \
	fprintf(stderr, "ERROR: Option %s needs argument!\n", arg.c_str());	\
	return 1;															\
  }																		\
  else {																\
	var = (float) atof(nextArg.c_str());								\
  }																		\

int main( int argc, char** argv )
{
  int depth = 7;

  Options options;

  for( int i = 1; i < argc; ++i ) {

    std::string arg( argv[i] );
    std::string nextArg = ( (i + 1) < argc ) ? std::string( argv[i + 1] ) : std::string();
    
    if( arg == "-d" || arg == "-depth" || arg == "--depth" ) {

      if( !nextArg.size() ) {

		fprintf( stderr, "ERROR: Argument '%s' needs parameter!\n\n", arg.c_str() );
		exit(1);
      }
	  
      depth = atoi( nextArg.c_str() );
      if( depth < 1 || depth > 8 ) {

		fprintf( stderr, "ERROR: Depth needs to be: 1 <= depth <= 8\n\n" );
		exit(1);
      }
      i++;
    }
	else if( arg == "-yaw" ) {

	  FLOATARG(options.yaw);
	}
	else if( arg == "-pitch" ) {
	  
	  FLOATARG(options.pitch);
	}
	else if( arg == "-posx" ) {
	  
	  FLOATARG(options.posx);
	}
	else if( arg == "-posy" ) {
	  
	  FLOATARG(options.posy);
	}
	else if( arg == "-posz" ) {
	  
	  FLOATARG(options.posz);
	}
	else if( arg == "-far" ) {
	  
	  FLOATARG(options.far);
	}
  }

  Renderer renderer;
  if( !renderer.init() ) {

    fprintf( stderr, "ERROR: Renderer::init\n" );
    exit( 1 );
  }

  if( !platform_initInput() ) {

    fprintf( stderr, "ERROR: platform_initInput" );
    renderer.shutdown();
    exit( 1 );
  }

  
  Texture* texFont = Texture::load( "font0808.png" );
  if( !texFont ) {

    fprintf( stderr, "ERROR: Texture::load\n" );
    renderer.shutdown();
    exit( 1 );
  }

  Font* font = new Font( texFont, ' ', 60, 8, 8, 9, 1, 3, 65, 100, 3, 2 );

  renderLoadingScreen( &renderer, font );

  Texture* texTerrain = Texture::load( "texture.png" );
  if( !texTerrain ) {
    
    fprintf( stderr, "ERROR: Texture::load\n" );
    renderer.shutdown();
    exit( 1 );
  }
  
  {
//     {
//       const int s = 1 << depth;
//       Octree ot( depth );
//      
//       fprintf( stderr, "building...\n" );
//       //Uint32 ticks0 = SDL_GetTicks();
//      
//       for( int z = 0; z < s; ++z ) {
// 	for( int x = 0; x < s; ++x ) {
//	  
// 	  const float Frequency = 1.0f / 200.0f;
//	  
// 	  int h = ( SimplexNoise1234::noise( Frequency * x, Frequency * z ) + 1.0f ) * 0.5f * s;
// 	  if( h < 1 ) h = 1;
// 	  else if( h > s ) h = s;
//	  
// 	  for( int y = 0; y < h; ++y ) {
//	    
// 	    //assert( ot.getLeafNode( x, y, z, true ) );
// 	    Octree::Leaf* leaf = ot.getLeaf( x, y, z ); // FIXME: Faster way to iterate...
// 	    if( leaf ) {
//	      
// 	      leaf->blockType = 1;
// 	    }
// 	  }
// 	}
//       }
//       ot.updateObstruction();

      //     printf( "saving...\n" );
      //     ticks0 = SDL_GetTicks();
      //     ot.savePlain( "o8.dat" );
      //     printf( "%u ms\n", SDL_GetTicks() - ticks0 );
      
    //fprintf( stderr, "rendering...\n" );
    
    #if USE_PLATFORM == PLATFORM_WIZ
    WizFlyByControls controls;
    #elif USE_PLATFORM == PLATFORM_CAANOO
    CaanooFlyByControls controls;
    #elif USE_PLATFORM == PLATFORM_PC
    PCFlyByControls controls;
    #else
    #error Unknown platform
    #endif

    //PagedTerrain terrain( depth, new SimpleHeightMapGenerator( 2 * 1 << depth ) );
    NonPagedTerrain terrain( depth, new SimpleHeightMapGenerator( 1 << depth ) );
    if( terrain.init() ) {
      
      renderLoop( &renderer, &terrain, font, texTerrain, &controls, options );
      terrain.shutdown();
    }
    else {

      fprintf( stderr, "ERROR: Terrain::init\n" );
      renderer.shutdown();
      exit(1);
    }
  }
  
  renderer.shutdown();

  return 0;
}
