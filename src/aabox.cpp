#include "aabox.h"

const char* AABox::faceToString( int face )
{
  switch( face ) {
    
  case FaceBack:
    return "BAK";
  case FaceFront:
    return "FRT";
  case FaceRight:
    return "RGT";
  case FaceLeft:
    return "LFT";
  case FaceTop:
    return "TOP";
  case FaceBottom:
    return "BOT";
  }
  
  return "---";
}

AABox::AABox( const Vector3f& vMin, float s )
  : vMin( vMin ), vMax( vMin[0] + s, vMin[1] + s, vMin[2] + s )
{
}

AABox::AABox( const Vector3f& vMin, const Vector3f& vMax )
  : vMin(vMin), vMax(vMax)
{
}

int AABox::getNearestFace( const Vector3f& pos ) const
{
  float min_d = std::abs( pos[2] - vMin[2] );
  int min_i = FaceBack;
    
  float d;
  d = std::abs( pos[2] - vMax[2] );
  if( d < min_d ) {
    
    min_d = d;
    min_i = FaceFront;
  }
  
  d = std::abs( pos[0] - vMax[0] );
  if( d < min_d ) {
    
    min_d = d;
    min_i = FaceRight;
  }
  
  d = std::abs( pos[0] - vMin[0] );
  if( d < min_d ) {
    
    min_d = d;
    min_i = FaceLeft;
  }
  
  d = std::abs( pos[1] - vMax[1] );
  if( d < min_d ) {
    
    min_d = d;
    min_i = FaceTop;
  }
  
  d = std::abs( pos[1] - vMin[1] );
  if( d < min_d ) {
    
    return FaceBottom;
  }
  
  return min_i;
}

bool AABox::sweep( const Vector3f& rayPos, const Vector3f& rayDir,
		   float* tIn, float* tOut ) const
{
    *tIn = -(std::numeric_limits<float>::max)();
    *tOut = (std::numeric_limits<float>::max)();
    float t0, t1;
    const float epsilon(0.0000001);

    // YZ plane.
    if ( std::abs( rayDir[0] ) < epsilon )
    {
      // Ray parallel to plane.
      if ( rayPos[0] < vMin[0] || rayPos[0] > vMax[0] )
      {
	return false;
      }
    }

    // XZ plane.
    if ( std::abs(rayDir[1]) < epsilon )
    {
      // Ray parallel to plane.
      if ( rayPos[1] < vMin[1] || rayPos[1] > vMax[1] )
      {
	return false;
      }
    }

    // XY plane.
    if ( std::abs(rayDir[2]) < epsilon )
    {
      // Ray parallel to plane.
      if ( rayPos[2] < vMin[2] || rayPos[2] > vMax[2] )
      {
	return false;
      }
    }

    // YZ plane.
    t0 = (vMin[0] - rayPos[0]) / rayDir[0];
    t1 = (vMax[0] - rayPos[0]) / rayDir[0];
    
    if ( t0 > t1 )
    {
      std::swap(t0, t1);
    }
      
    if ( t0 > *tIn )
    {
      *tIn = t0;
    }
    if ( t1 < *tOut )
    {
      *tOut = t1;
    }

    if ( *tIn > *tOut || *tOut < float(0) )
    {
      return false;
    }

    // XZ plane.
    t0 = (vMin[1] - rayPos[1]) / rayDir[1];
    t1 = (vMax[1] - rayPos[1]) / rayDir[1];
    
    if ( t0 > t1 )
    {
      std::swap(t0, t1);
    }

    if ( t0 > *tIn )
    {
      *tIn = t0;
    }
    if ( t1 < *tOut )
    {
      *tOut = t1;
    }

    if ( *tIn > *tOut || *tOut < float(0) )
    {
      return false;
    }

    // XY plane.
    t0 = (vMin[2] - rayPos[2]) / rayDir[2];
    t1 = (vMax[2] - rayPos[2]) / rayDir[2];

    if ( t0 > t1 )
    {
      std::swap(t0, t1);
    }

    if ( t0 > *tIn )
    {
      *tIn = t0;
    }
    if ( t1 < *tOut )
    {
      *tOut = t1;
    }

    if ( *tIn > *tOut || *tOut < float(0) )
    {
      return false;
    }

    return true;
}

bool AABox::sweep( const AABox& otherBox, const Vector3f& otherDir,
		   float* toi /*, float* tOut*/ ) const
{
//   if( intersects(otherBox) ) {
//
//     *tIn = *tOut = 0;
//     return true;
//   }

  //if( otherDir.squaredNorm() < 0.001f )
  //  return false;


  Vector3f overlap1( 1.0f, 1.0f, 1.0 );
  Vector3f overlap2( 0.0f, 0.0f, 0.0 );

  for( int i = 0; i < 3; ++i ) {

    // http://www.rgrjr.com/emacs/overlap.html
    // Two intervals i1 = (s1, e1) and i2 = (s2, e2) overlap if and only if
    // s2 < e1 and s1 < e2
    
    if( otherDir[i] < 0 ) {

      if( otherBox.vMax[i] <= vMin[i] )
	return false;

      overlap1[i] = (otherBox.vMin[i] - vMax[i]) / -otherDir[i];
      overlap2[i] = (otherBox.vMax[i] - vMin[i]) / -otherDir[i];
    }
    else if( otherDir[i] > 0 ) {

      if( otherBox.vMin[i] >= vMax[i] )
	return false;

      overlap1[i] = (vMin[i] - otherBox.vMax[i]) / otherDir[i];
      overlap2[i] = (vMax[i] - otherBox.vMin[i]) / otherDir[i];
    }
    else if( otherBox.vMin[i] < vMax[i] && vMin[i] < otherBox.vMax[i] ) {
     
      overlap1[i] = 0.0f;
      overlap2[i] = 1.0f;
    }
    else {

      return false;
    }
  }

  float max1 = std::max( overlap1[0], std::max( overlap1[1], overlap1[2] ) );
  float min2 = std::min( overlap2[0], std::min( overlap2[1], overlap2[2] ) );

  if( max1 >= 1.0f )
    return false;

  if( max1 >= min2 )
    return false;

  *toi = max1;
  return true;
}

bool AABox::intersects( const AABox& otherBox ) const
{
  for( int i = 0; i < 3; ++i ) {

    if( vMin[i] > otherBox.vMax[i] )
      return false;
  }
  
  for( int i = 0; i < 3; ++i ) {

    if( otherBox.vMin[i] > vMax[i] )
      return false;
  }

  return true;
}
