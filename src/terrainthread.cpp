#include "octree.h"
#include "terrain.h"
#include "terraingenerator.h"
#include "terrainpage.h"

TerrainThread::TerrainThread( AbstractTerrainGenerator* generator )
  : _generator( generator )
{
  pthread_mutex_init( &_queueInMutex, 0 );
  pthread_cond_init( &_queueInCond, 0 );
}

bool TerrainThread::start()
{
  // TODO: Priority...
  int err = pthread_create( &_thread, 0, callback, this );
  return err == 0;
}

void TerrainThread::stop()
{
  beginSend();
  sendQuit();
  endSend();
  pthread_join( _thread, 0 );
}

void TerrainThread::beginSend()
{
  pthread_mutex_lock( &_queueInMutex );
}

void TerrainThread::endSend()
{
  pthread_cond_signal( &_queueInCond );
  pthread_mutex_unlock( &_queueInMutex );
}

void TerrainThread::sendRequest( const Request& req )
{
  _queueIn.push_back( req );
}

void TerrainThread::sendQuit()
{
  Request req;
  req.type = Request::Quit;
  sendRequest( req );
}

void TerrainThread::sendGeneratePage( TerrainPage* page, int depth )
{
  Request req;
  req.type = Request::GeneratePage;
  req.page = page;
  req.depth = depth;
  sendRequest( req );
}

void* TerrainThread::callback( void* arg )
{
  static_cast< TerrainThread* >( arg )->run();
  return 0;
}

void TerrainThread::run()
{
  bool doQuit = false;
  while( !doQuit ) {
    
    pthread_mutex_lock( &_queueInMutex );
    
    while( !_queueIn.size() )
      pthread_cond_wait( &_queueInCond, &_queueInMutex );
    
    Request req = _queueIn.front();
    _queueIn.pop_front();
    
    pthread_mutex_unlock( &_queueInMutex );
    
    switch( req.type ) {
      
    case Request::Quit:
      doQuit = true;
      break;

    case Request::GeneratePage:
      {
	_generator->generate( req.page->octree, 
			      req.page->curr_ox,
			      req.page->curr_oy,
			      req.page->curr_oz );
	
	req.page->state = TerrainPage::Returned;
      }
      break;
    }
  }
}

