#include "aabox.h"
#include "camera.h"
#include "facemask.h"
#include "font.h"
#include "octree.h"
#include "renderer.h"
#include "texture.h"

#include <SDL.h>

#include <gl.h>

#include <stdint.h>
#include <string.h>

// static bool hit1D( float rayX, float rayDX, float planeX, float planeDX )
// {
//   if( rayX > planeX )
//     return rayDX < 0 && planeDX > 0;
//   else if( rayX < planeX )
//     return dayDX > 0 && planeDX < 0;
//   else
//     return false;
// }

// static float rayPlaneDistance( const Vector3f& rayStart, const Vector3f& rayDir,
// 			       const Vector3f& planeNormal, float planeD )
// {
//   float cosA = rayDir.dot( planeNormal );
//   if( cosA == 0.0f )
//     return -1.0f;
  
//   return ( planeD - rayStart.dot( planeNormal ) ) / cosA;
// }


// static void rayAACubeDistance( const Vector3f& pos, const Vector3f& dir, int x, int y, int z, int s, float* d, int* face )
// {
//   Vector3f planeN[6];
//   planeN[FaceBack]   =  Vector3f::UnitZ();
//   planeN[FaceFront]  = -Vector3f::UnitZ();
//   planeN[FaceRight]  =  Vector3f::UnitX();
//   planeN[FaceLeft]   = -Vector3f::UnitX();
//   planeN[FaceTop]    =  Vector3f::UnitY();
//   planeN[FaceBottom] = -Vector3f::UnitY();

//   Vector3f planeP[6];
//   planeP[FaceBack]   = Vector3f(     x,     y, z + s );
//   planeP[FaceFront]  = Vector3f(     x,     y,     z );
//   planeP[FaceRight]  = Vector3f( x + s,     y,     z );
//   planeP[FaceLeft]   = Vector3f(     x,     y,     z );
//   planeP[FaceTop]    = Vector3f(     x, y + s,     z );
//   planeP[FaceBottom] = Vector3f(     x,     y,     z );

//   int count = 0;

//   float minD = 1000.0f;
//   int minI = -1;
//   for( int i = 0; i < 6; ++i ) {

//     float planeD = -planeN[i].dot( planeP[i] );
    
//     float d = rayPlaneDistance( pos, dir, planeN[i], planeD );
//     if( d <= 0 )
//       continue;

//     if( d < minD ) {
      
//       minD = d;
//       minI = i;
//     }

//     count++;
//     if( count > 1 )
//       break;
//   }
  
//   if( count > 1 ) {

//     *d = minD;
//     *face = minI;
//   }
//   else {

//     *d = -1.0f;
//     *face = -1;
//   }
// }

Octree::Node::Node( /*Node* parent,*/ unsigned char depth )
  : /*_parent( parent ),*/ _depth( depth ), _culled( false )
{
//   assert( depth > 0 );

//   if( parent )
//     assert( _depth < parent->_depth );

  if( _depth > 1 ) {

    for( int i = 0; i < 8; ++i )
      _children[i] = new Node( /*this,*/ depth - 1 );
  }
  else {
    
    memset( _leafs, 0, sizeof( _leafs ) );
  }
}

Octree::Node::~Node()
{
  if( _depth > 1 ) {

    for( int i = 0; i < 8; ++i )
      delete _children[i];
  }
}

TerrainLeaf* Octree::Node::getLeaf( int x, int y, int z )
{
  const int halfSize = getSize() / 2;
  int offsetZ = (z < halfSize) ? 0 : 4;
  int offsetY = (y < halfSize) ? 0 : 2;
  int offsetX = (x < halfSize) ? 0 : 1;
  int i = offsetZ + offsetY + offsetX;

  if( _depth > 1 ) {
     
    Node* child = _children[ i ];
    return child->getLeaf( x % halfSize, y % halfSize, z % halfSize );
  }
  else {

    return &_leafs[i];
  }
}

static bool writeU32( FILE* fp, uint32_t value )
{
  return 
    ( fputc( value         & 0xFF, fp ) != EOF ) &&
    ( fputc( (value >>  8) & 0xFF, fp ) != EOF ) &&
    ( fputc( (value >> 16) & 0xFF, fp ) != EOF ) &&
    ( fputc( (value >> 24) & 0xFF, fp ) != EOF );
}

bool Octree::Node::savePlain( FILE* fp, int count )
{
//   if( _depth == 0 ) {
//
//     //fputc( (int) count, fp );
//     writeU32( fp, count );
//     return true;
//   }
//
//   int s = getSize();
//   int c = ( s * s * s ) / 8;
//  
//   for( int i = 0; i < 8; ++i ) {
//    
//     if( _children[i] && !_children[i]->savePlain( fp, count ) )
//       return false;
//
//     count += c;
//   }
//
//   return true;

  return false;
}

//static const float SQRT2 = sqrtf( 2.0f );

void Octree::Node::frustumCull( Camera* camera, int x, int y, int z, int* numCulled )
{
  if( _depth < 2 )
    return;
  
  const int size = (int) getSize();
  const int halfSize = size / 2;

  _culled = 
    ( !camera->isSphereInFrustum( Vector3f( x + halfSize, y + halfSize, z + halfSize ), 0.866f * size ) ) ||
    ( !camera->isAACubeInFrustum( Vector3f( x, y, z ), size ) );

  if( _culled ) {
    
    *numCulled += /*(size*size*size)*/ getNumCubes();
    return;
  }
  
  if( _children[0] )
    _children[0]->frustumCull( camera, x, y, z,
			       numCulled);
  if( _children[1] )
    _children[1]->frustumCull( camera, x + halfSize, y, z, 
			       numCulled );
  if( _children[2] )
    _children[2]->frustumCull( camera, x, y + halfSize, z,
			       numCulled );
  if( _children[3] )
    _children[3]->frustumCull( camera, x + halfSize, y + halfSize, z,
			       numCulled );
  if( _children[4] )
    _children[4]->frustumCull( camera, x, y, z + halfSize,
			       numCulled );
  if( _children[5] )
    _children[5]->frustumCull( camera, x + halfSize, y, z + halfSize,
			       numCulled );
  if( _children[6] )
    _children[6]->frustumCull( camera, x, y + halfSize, z + halfSize,
			       numCulled );
  if( _children[7] )
    _children[7]->frustumCull( camera, x + halfSize, y + halfSize, z + halfSize,
			       numCulled );
}

// static void renderCube( int x, int y, int z )
// {
//   float w = 1.0;
//
//   glBegin( GL_QUADS );
//
//   // Front
//   glVertex3f(     x,     y, z );
//   glVertex3f(     x, y + w, z );
//   glVertex3f( x + w, y + w, z );
//   glVertex3f( x + w,     y, z );
//
//   // Back
// //   glVertex3f( x + w,     y, z - w );
// //   glVertex3f( x + w, y + w, z - w );
// //   glVertex3f(     x, y + w, z - w );
// //   glVertex3f(     w, y + w, z - w );
//
//   glEnd();
// }

void Octree::Node::render( RenderState* renderState, int x, int y, int z )
{
  if( _culled )
    return;

  if( _depth == 1 ) {

    int l = 0;

    for( int k = 0; k < 2; ++k ) {
      for( int j = 0; j < 2; ++j ) {
	for( int i = 0; i < 2; ++i ) {
	
	  TerrainLeaf* leaf = &_leafs[l++];
	  if( !leaf->blockType )
	    continue;

	  const unsigned char faceFlags = ~leaf->obstruction;
	  if( !faceFlags )
		continue;

	  //faceFlags = 0xFF;

	  const float s = 1;
	  
	  Vector2f texCoordsTop[4];
	  texCoordsTop[0] = Vector2f( 32.0f/256.0f, 0.0f );
	  texCoordsTop[1] = Vector2f( 32.0f/256.0f, 16.0f/256.0f );
	  texCoordsTop[2] = Vector2f( 48.0f/256.0f, 16.0f/256.0f );
	  texCoordsTop[3] = Vector2f( 48.0f/256.0f, 0.0f );
	  
	  Vector2f texCoordsOther[4];
	  texCoordsOther[0] = Vector2f( 48.0f/256.0f, 0.0f );
	  texCoordsOther[1] = Vector2f( 48.0f/256.0f, 16.0f/256.0f );
	  texCoordsOther[2] = Vector2f( 64.0f/256.0f, 16.0f/256.0f );
	  texCoordsOther[3] = Vector2f( 64.0f/256.0f, 0.0f );
	  
	  Vector2f texCoordsBottom[4];
	  texCoordsBottom[0] = Vector2f( 64.0f/256.0f, 0.0f );
	  texCoordsBottom[1] = Vector2f( 64.0f/256.0f, 16.0f/256.0f );
	  texCoordsBottom[2] = Vector2f( 80.0f/256.0f, 16.0f/256.0f );
	  texCoordsBottom[3] = Vector2f( 80.0f/256.0f, 0.0f );
	  
	  Vector3f cubeVertices[8];
	  cubeVertices[0] = Vector3f(     x + i, y + s + j,     z + k );
	  cubeVertices[1] = Vector3f(     x + i, y + s + j, z + s + k );
	  cubeVertices[2] = Vector3f( x + s + i, y + s + j, z + s + k );
	  cubeVertices[3] = Vector3f( x + s + i, y + s + j,     z + k );
	  
	  cubeVertices[4] = Vector3f(     x + i,     y + j,     z + k );
	  cubeVertices[5] = Vector3f(     x + i,     y + j, z + s + k );
	  cubeVertices[6] = Vector3f( x + s + i,     y + j, z + s + k );
	  cubeVertices[7] = Vector3f( x + s + i,     y + j,     z + k );
	  
	  if( faceFlags & FaceFront ) {
	    
	    Vector3f vertices[4];
	    vertices[0] = cubeVertices[1];
	    vertices[1] = cubeVertices[5];
	    vertices[2] = cubeVertices[6];
	    vertices[3] = cubeVertices[2];
	    
	    Vector3f normal = Vector3f::UnitZ();
	    renderState->addRect( vertices, texCoordsOther, &normal );
	  }
	  if( faceFlags & FaceBack ) {
	    
	    Vector3f vertices[4];
	    vertices[0] = cubeVertices[3];
	    vertices[1] = cubeVertices[7];
	    vertices[2] = cubeVertices[4];
	    vertices[3] = cubeVertices[0];
	    
	    Vector3f normal = Vector3f::UnitZ() * -1.0f;
	    renderState->addRect( vertices, texCoordsOther, &normal );
	  }
	  if( faceFlags & FaceLeft ) {
	    
	    Vector3f vertices[4];
	    vertices[0] = cubeVertices[0];
	    vertices[1] = cubeVertices[4];
	    vertices[2] = cubeVertices[5];
	    vertices[3] = cubeVertices[1];
	    
	    Vector3f normal = Vector3f::UnitX() * -1.0f;
	    renderState->addRect( vertices, texCoordsOther, &normal );
	  }
	  if( faceFlags & FaceRight ) {
	    
	    Vector3f vertices[4];
	    vertices[0] = cubeVertices[2];
	    vertices[1] = cubeVertices[6];
	    vertices[2] = cubeVertices[7];
	    vertices[3] = cubeVertices[3];
	    
	    Vector3f normal = Vector3f::UnitX();
	    renderState->addRect( vertices, texCoordsOther, &normal );
	  }
	  if( faceFlags & FaceTop ) {
	    
	    Vector3f vertices[4];
	    vertices[0] = cubeVertices[0];
	    vertices[1] = cubeVertices[1];
	    vertices[2] = cubeVertices[2];
	    vertices[3] = cubeVertices[3];
	    
	    Vector3f normal = Vector3f::UnitY();
	    renderState->addRect( vertices, texCoordsTop, &normal );
	  }
	  if( faceFlags & FaceBottom ) {
	    
	    Vector3f vertices[4];
	    vertices[0] = cubeVertices[5];
	    vertices[1] = cubeVertices[4];
	    vertices[2] = cubeVertices[7];
	    vertices[3] = cubeVertices[6];
	    
	    Vector3f normal = Vector3f::UnitY() * -1.0f;
	    renderState->addRect( vertices, texCoordsBottom, &normal );
	  }
	}
      }
    }
  }
  else {

    const int halfSize = (int) getSize() / 2;
    
    if( _children[0] )
      _children[0]->render( renderState,            x,            y, z );
    if( _children[1] )
      _children[1]->render( renderState, x + halfSize,            y, z );
    if( _children[2] )
      _children[2]->render( renderState,            x, y + halfSize, z );
    if( _children[3] )
      _children[3]->render( renderState, x + halfSize, y + halfSize, z );

    if( _children[4] )
      _children[4]->render( renderState,            x,            y, z + halfSize );
    if( _children[5] )
      _children[5]->render( renderState, x + halfSize,            y, z + halfSize );
    if( _children[6] )
      _children[6]->render( renderState,            x, y + halfSize, z + halfSize );
    if( _children[7] )
      _children[7]->render( renderState, x + halfSize, y + halfSize, z + halfSize );
  }
}

static void fillChildCoords( const int x, const int y, const int z, const int hs, float* vx, float* vy, float* vz )
{
  vx[0] = x;
  vx[1] = x + hs;
  vx[2] = x;
  vx[3] = x + hs;
  vx[4] = x;
  vx[5] = x + hs;
  vx[6] = x;
  vx[7] = x + hs;

  vy[0] = y;
  vy[1] = y;
  vy[2] = y + hs;
  vy[3] = y + hs;
  vy[4] = y;
  vy[5] = y;
  vy[6] = y + hs;
  vy[7] = y + hs;

  vz[0] = z;
  vz[1] = z;
  vz[2] = z;
  vz[3] = z;
  vz[4] = z + hs;
  vz[5] = z + hs;
  vz[6] = z + hs;
  vz[7] = z + hs;
}

void Octree::Node::castRay( const Vector3f& pos, const Vector3f& dir, 
			    int ox, int oy, int oz,
			    TerrainLeaf** leaf, int* lx, int* ly, int* lz, /*int *face,*/ float* d )
{
  {
    AABox cube( Vector3f(ox,oy,oz), getSize() );
    float t0;
    float t1;
    
    if( !cube.sweep( pos, dir, &t0, &t1 ) ) {
      
      return;
    }
  }

  if( _depth > 1 ) {

    *d = 1000.0f;

    TerrainLeaf* local_leaf = 0;
    int local_lx;
    int local_ly;
    int local_lz;
    float local_d;

    float cx[8];
    float cy[8];
    float cz[8];
    fillChildCoords( ox, oy, oz, getSize() / 2, cx, cy, cz );

    for( int i = 0; i < 8; ++i ) {

      _children[i]->castRay( pos, dir, cx[i], cy[i], cz[i], 
			     &local_leaf, &local_lx, &local_ly, &local_lz, &local_d );

      if( local_leaf && local_d < *d ) {

	*d = local_d;
	
	*leaf = local_leaf;
	*lx = local_lx;
	*ly = local_ly;
	*lz = local_lz;
      }
    }
  }
  else {

    *d = 1000.0f;

    float cx[8];
    float cy[8];
    float cz[8];
    fillChildCoords( ox, oy, oz, getSize() / 2, cx, cy, cz );

    for( int i = 0; i < 8; ++i ) {
    
      float t0;
      float t1;

      if( !_leafs[i].blockType )
	continue;

      AABox cube( Vector3f( cx[i], cy[i], cz[i] ), 1.0f );
      if( !cube.sweep( pos, dir, &t0, &t1 ) )
	continue;

      if( t0 < *d ) {

	*d = t0;

	*leaf = &_leafs[i];
	*lx = cx[i];
	*ly = cy[i];
	*lz = cz[i];
      }
    }
  }
}

void Octree::Node::testCollision( const AABox& boxIn, const Vector3f& dpIn, 
				  int xIn, int yIn, int zIn,
				  std::vector< TerrainLeafCollision >* collisionsOut )
{
  // See if cube collides with this node...
  {
    AABox cube = getAACube(xIn,yIn,zIn);
    
    float tEnter;
    //float tLeave;
    
    if( !cube.sweep( boxIn, dpIn, &tEnter /*, &tLeave*/ ) )
      return;
  }

  // Check children...
  if( _depth > 1 ) {

    float cx[8];
    float cy[8];
    float cz[8];
    fillChildCoords( xIn, yIn, zIn, getSize() / 2, cx, cy, cz );

    for( int i = 0; i < 8; ++i ) {
      
      _children[i]->testCollision( boxIn, dpIn,
				   cx[i], cy[i], cz[i],
				   collisionsOut );
    }
  }
  else {

    float cx[8];
    float cy[8];
    float cz[8];
    fillChildCoords( xIn, yIn, zIn, getSize() / 2, cx, cy, cz );

    float tEnter;
    //float tLeave;

    for( int i = 0; i < 8; ++i ) {
      
      // Don't check empty blocks...
      if( !_leafs[i].blockType )
	continue;
      
      AABox cube( Vector3f( cx[i], cy[i], cz[i] ), 1.0f );
      if( !cube.sweep( boxIn, dpIn, &tEnter/*, &tLeave*/ ) ) 
	continue;
      
//    if( tEnter > 1.0f  )
// 	continue; // Collision happens in the future...
//
//    if( tLeave - tEnter < 0.1f )
//      continue; // Release is imminent...

      collisionsOut->push_back(TerrainLeafCollision( &_leafs[i], cx[i], cy[i], cz[i], tEnter /*, tLeave*/ ) );
    }
  }
}

AABox Octree::Node::getAACube( int x, int y, int z ) const
{
  return AABox( Vector3f(x,y,z), getSize() );
}

Octree::Octree( unsigned char depth )
{
  _depth = depth;
  _root = new Node( /*0,*/ depth );
}

Octree::~Octree()
{
  delete _root;
}

TerrainLeaf* Octree::getLeaf( int x, int y, int z )
{
  const int size = getSize();
  
  if( x < 0 || x >= size ||
      y < 0 || y >= size || 
      z < 0 || z >= size ) {
    
    return 0;
  }
  
  return _root->getLeaf( x, y, z );
}

bool Octree::savePlain( const char* filename )
{
  FILE* fp = fopen( filename, "w+" );
  if( !fp ) {

    perror( "fopen" );
    return false;
  }

  //return _root->savePlain( fp, 0 );

  const int size = getSize();
  for( int i = 0; i < size; ++i ) {
    for( int j = 0; j < size; ++j ) {
      for( int k = 0; k < size; ++k ) {

	TerrainLeaf* n = getLeaf( i, j, k );
	if( !writeU32( fp, n ? 1 : 0 ) ) {

	  perror( "putc" );
	  fclose(fp);
	  return false;
	}
      }
    }
  }

  fclose(fp);
  return true;
}

void Octree::frustumCull( Camera* camera, int ox, int oy, int oz, int* numCulled )
{
  *numCulled = 0;
  
  _root->frustumCull( camera, ox, oy, oz, numCulled );
}

void Octree::render( /*Renderer* renderer, Texture* texture*/ RenderState* renderState, int ox, int oy, int oz )
{
  _root->render( renderState, ox, oy, oz );
}

void Octree::updateObstruction()
{
  const int size = 1 << _depth;
  for( int k = 0; k < size; ++k ) {
    for( int j = 0; j < size; ++j ) {
      for( int i = 0; i < size; ++i ) {

	TerrainLeaf* leaf = getLeaf( i, j, k );
	if( leaf )
	  leaf->updateObstruction( this, i, j, k );
      }
    }
  }
}

void Octree::clear()
{
  delete _root;
  _root = new Node( _depth );
}

void Octree::castRay( const Vector3f& pos, const Vector3f& dir, 
		      int ox, int oy, int oz,
		      TerrainLeaf** leaf, int* lx, int* ly, int* lz, /*int *face,*/ float* d )
{
  return _root->castRay( pos, dir, ox, oy, oz, leaf, lx, ly, lz, /*face,*/ d );
}

void Octree::testCollision( const AABox& boxIn, const Vector3f& dpIn, 
			    int xIn, int yIn, int zIn,
			    std::vector< TerrainLeafCollision >* collisionsOut )
{
  _root->testCollision( boxIn, dpIn, xIn, yIn, zIn,
			collisionsOut );
}
