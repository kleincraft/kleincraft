#include "facemask.h"
#include "octree.h"
#include "terrainleaf.h"

static void setNeighborMask( TerrainLeaf* neighbor, 
							 const bool isObstructed,
							 const unsigned char mask )
{
  if( neighbor ) {

	if( isObstructed ) {

	  neighbor->obstruction |= mask;
	}
	else {

	  neighbor->obstruction &= ~mask;
	}
  }
}

void TerrainLeaf::updateNeighborsObstruction( Octree* octree, 
											  const int x, 
											  const int y, 
											  const int z )
{
  const bool isSolid = (blockType != 0);

  setNeighborMask( octree->getLeaf( x - 1, y, z ), isSolid, FaceRight );
  setNeighborMask( octree->getLeaf( x + 1, y, z ), isSolid, FaceLeft );
  setNeighborMask( octree->getLeaf( x, y - 1, z ), isSolid, FaceTop );
  setNeighborMask( octree->getLeaf( x, y + 1, z ), isSolid, FaceBottom );
  setNeighborMask( octree->getLeaf( x, y, z - 1 ), isSolid, FaceFront );
  setNeighborMask( octree->getLeaf( x, y, z + 1 ), isSolid, FaceBack );
}

static void setSelfMask( TerrainLeaf* self,
						 const TerrainLeaf* neighbor,
						 const unsigned char mask )
{
  const bool isObstructed = neighbor && (neighbor->blockType != 0);
  
  if( isObstructed ) {

	self->obstruction |= mask;
  }
  else {

	self->obstruction &= ~mask;
  }
}

void TerrainLeaf::updateObstruction( Octree* octree, int x, int y, int z )
{
  setSelfMask(this, octree->getLeaf( x - 1, y, z ), FaceLeft );
  setSelfMask(this, octree->getLeaf( x + 1, y, z ), FaceRight );
  setSelfMask(this, octree->getLeaf( x, y - 1, z ), FaceBottom );
  setSelfMask(this, octree->getLeaf( x, y + 1, z ), FaceTop );
  setSelfMask(this, octree->getLeaf( x, y, z - 1 ), FaceBack );
  setSelfMask(this, octree->getLeaf( x, y, z + 1 ), FaceFront );
}

void TerrainLeaf::setBlockType( Octree* octree, int x, int y, int z, uint8_t bt )
{
  if( bt != blockType ) {

	blockType = bt;
	updateNeighborsObstruction(octree, x, y, z);
  }
}
