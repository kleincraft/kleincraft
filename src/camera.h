#ifndef __CAMERA_H__
#define __CAMERA_H__

#include <Eigen/Core>
using namespace Eigen;

#include <gl.h>

#include <math.h>

class Camera
{
  // http://www.lighthouse3d.com/opengl/viewfrustum/index.php?intro

 public:

  enum {

    NearPlane,
    FarPlane,
    LeftPlane,
    RightPlane,
    TopPlane,
    BottomPlane
  };
  
  Camera();

  //! Sets up frustum
  void setupFrustum( GLfloat fov, GLfloat ratio, GLfloat nearD, GLfloat farD );

  void applyFrustum();

  //! Sets camera position and orientation
  void set( const Vector3f& pos, float yaw, float pitch );

  //! Moves/Rotates the camera
  void move( float dX, float dY, float dZ, float dYaw, float dPitch );

  //! Applies translation/rotation to model view matrix...
  void applyCamera();

  //! Checks for frustum culling...
  bool isSphereInFrustum( const Vector3f& pos, float radius ) /*const*/;
  bool isAACubeInFrustum( const Vector3f& pos, float size ) /*const*/;

  float getYaw() const;
  float getPitch() const;

  const Vector3f& getPos() const { return _pos; }
  void setPos( const Vector3f& pos ) { _pos = pos; _dirty = true; }

  const Vector3f& getDir() const { return _dir; }

 private:

  //! Calculates frustum planes for culling.
  //! Needs to be called after camera was moved.
  void calcFrustum();
  void normalizeYawPitch();
  void calcDir();

  bool _dirty;

  Vector3f _pos;
  Vector3f _dir;
  Vector3f _right;
  Vector3f _up;

  float _frustumFov;
  float _frustumRatio;

  float _nearH;
  float _nearW;
  float _nearD;

  float _farH;
  float _farW;
  float _farD;

  Vector3f _frustumP[6];
  Vector3f _frustumN[6];
  float _frustumD[6];

  float _yaw;
  float _pitch;
};

#endif // __CAMERA_H__
