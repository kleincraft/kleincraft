#ifndef __VERSION_H__
#define __VERSION_H__

#include <string>

std::string getVersionString();

#endif // __VERSION_H__
