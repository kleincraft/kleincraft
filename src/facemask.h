#ifndef _FACEMASK_H_
#define _FACEMASK_H_

enum FaceMask {
    
  FaceFront   =  1,
  FaceBack    =  2, 
  FaceLeft    =  4,
  FaceRight   =  8,
  FaceTop     = 16,
  FaceBottom  = 32,
  
  FaceAll = 
    FaceFront 
  | FaceBack
  | FaceLeft
  | FaceRight
  | FaceTop 
  | FaceBottom
};

#endif /* _FACEMASK_H_ */
