#ifndef __AABOX_H__
#define  __AABOX_H__

#include <Eigen/Core>
using namespace Eigen;

struct AABox
{
  // Adapted from Generic Math Template Library (http://ggt.sourceforge.net/)
  
  enum Face
  {
    FaceBack,
    FaceFront,
    FaceRight,
    FaceLeft,
    FaceTop,
    FaceBottom
  };

  static const char* faceToString( int face );

  AABox( const Vector3f& vMin, float s );
  AABox( const Vector3f& vMin, const Vector3f& vMax );

  int getNearestFace( const Vector3f& pos ) const;

  bool sweep( const Vector3f& rayPos, const Vector3f& rayDir,
	      float* tIn, float* tOut ) const;

  bool sweep( const AABox& otherBox, const Vector3f& otherDir,
	      float* toi /*, float* tOut*/ ) const;

  bool intersects( const AABox& otherBox ) const;
		   
  Vector3f vMin;
  Vector3f vMax;
};

#endif //  __AABOX_H__
