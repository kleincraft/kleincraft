#ifndef __RENDERER_H__
#define __RENDERER_H__

#include <gl.h>
#include <Eigen/Core>

#include <stdio.h>

class Texture;

#define RENDERER_GL   1
#define RENDERER_GLES 2

class Renderer
{
 public:
  bool init();
  void startFrame();
  void renderFrame();

  void addNumTrisRendered( unsigned value );
  unsigned getNumTrisRendered() const { return _numTrisRendered; }

  bool initScreen();
  void shutdown();

  void renderColoredCube( GLfloat x, GLfloat y, GLfloat z, GLfloat size, 
			  GLfloat r, GLfloat g, GLfloat b, GLfloat a );

 private:
  unsigned _numTrisRendered;
};

class RenderState
{
 public:

  static const unsigned TrisPerBatch = 0x600;
  static const unsigned VerticesPerBatch = TrisPerBatch * 3;
  static const unsigned BufferLength = VerticesPerBatch * 3 + VerticesPerBatch * 2 + VerticesPerBatch * 3;

  RenderState( Renderer* renderer );
  ~RenderState();

  //void addRect( Vertex3f verts[4], Vectex3f normal );
  //void addTexturedRect( Vertex3f verts[4], Vertex2f texCoords[4], Vectex3f normal );

  virtual void setup() = 0;

  void flush();

  bool canAddRect() const 
  { 
    //return (_vertexPtr - _vertices) + ( n * 3  * 3 ) <= VerticesPerBatch * 3;
    return _bufferI <= BufferLength - 48;
  }

  //GLfloat** getVertexPtrPtr() { return &_vertexPtr; }
  //GLfloat** getNormalPtrPtr() { return &_normalPtr; }

//   void addRect( const GLfloat* vertices, 
// 		const GLfloat* texCoords,
// 		const GLfloat* normal );

//   void addRect2D( const GLfloat* vertices, 
// 		  const GLfloat* texCoords );
  
  void addRect( const Eigen::Vector3f* vertices, 
		const Eigen::Vector2f* texCoords,
		const Eigen::Vector3f* normal );
  
  void addRect2D( const Eigen::Vector2f* vertices, 
		  const Eigen::Vector2f* texCoords );

//   void addTris( unsigned n, const GLfloat* vertices, 
// 		const GLfloat* texCoords,
// 		const GLfloat* normals );

//   void addTris( unsigned n, 
// 		const Eigen::Vector3f* vertices, 
// 		const Eigen::Vector2f* texCoords,
// 		const Eigen::Vector3f* normals );

 private:
  Renderer* _renderer;

//   Mode _mode;
//   GLuint _texture;

  GLfloat _buffer[ BufferLength ];
  unsigned int _bufferI;

  //GLfloat _vertices[ VerticesPerBatch * 3 ]; // 3D
  //GLfloat* _vertexPtr;
  
  //GLfloat _texCoords[ VerticesPerBatch * 2 ]; // 2D
  //GLfloat* _texCoordPtr;
  
  //GLfloat _normals[ VerticesPerBatch * 3 ]; // One normal per tri, 3D
  //GLfloat* _normalPtr;
};


class TerrainRenderState : public RenderState
{
public:
  TerrainRenderState( Renderer* renderer, Texture* texture );
  void setup();

private:
  GLuint _textureId;
};

class FontRenderState : public RenderState
{
public:
  FontRenderState( Renderer* renderer, Texture* texture );
  void setup();

private:
  GLuint _textureId;
};

#endif // __RENDERER_H__
