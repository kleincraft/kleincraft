#ifndef __GLES11_H__
#define __GLES11_H__

#include "renderer.h"
#if( USE_RENDERER == RENDERER_GLES )

//! OpenGL ES 1.1 support code...
namespace GLES11
{
  bool initGLES();
  void shutdownGLES();
  void swapBuffers();
};

#endif // RENDERER_GLES

#endif // __GLES11_H__
