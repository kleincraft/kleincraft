#include "octree.h"
#include "terrainpage.h"

void TerrainPage::frustumCull( Camera* camera )
{
  //if( !isActive() )
  //  return;
  
  int numCulled = 0;
  octree->frustumCull( camera, curr_ox, curr_oy, curr_oz, &numCulled );
}

void TerrainPage::render( RenderState* renderState )
{
  //if( !isActive() )
  //  return;
  
  //octree->render( renderState, ox, oy, oz );
}

