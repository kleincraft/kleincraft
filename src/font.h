#ifndef __FONT_H__
#define __FONT_H__

#include <Eigen/Core>

class Renderer;
class Texture;

class Font
{
 public:

  Font( Texture* texture, 
	int firstChar,
	int numChars,
	int charW,
	int charH,
	int advanceX,
	int areaX,
	int areaY,
	int areaW,
	int areaH,
	int gapW,
	int gapH );

  ~Font();

  Eigen::Vector2f printf( Renderer* renderer, const Eigen::Vector2f& pos0, const char* format, ... );
  Eigen::Vector2f print( Renderer* renderer, const Eigen::Vector2f& pos0, const char* text );

 private:  

  struct Character
  {
    Eigen::Vector2f texCoords[4];
  };

  Texture* _texture;
  int _firstChar;
  int _numChars;
  int _charW;
  int _charH;
  int _advanceX;
  Character* _characters;
};

#endif // __FONT_H__
