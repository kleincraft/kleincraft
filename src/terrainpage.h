#ifndef __TERRAINPAGE_H__
#define __TERRAINPAGE_H__

class Camera;
class Octree;
class RenderState;

//! A page of terrain, contains one octree and global coordinates.
struct TerrainPage
{
  enum State {
    
    Null,
    
    AboutToBeSent, // Page is about to be sent to terrain thread
    Sent, // Page is sent to terrain thread
    Processing, // Page is being processed by terrain thread
    Returned, // Page is back from terrain thread
    
    Active, // Page is present and usable...
  };

  TerrainPage()
  {
    state = Null;
    octree = 0;
  }
  
  void frustumCull( Camera* camera );
  void render( RenderState* renderState );

  unsigned state;

  // Current coordinates of the page
  // Read by foreground thread when state is "Active"
  // Read by background thread when state is in "Sent" and "Processing"
  // Written by forground thread when state is "Returned"
  int curr_ox;
  int curr_oy;
  int curr_oz;

  // Next coordinates of the page
  // Written by foreground thread when state is "AboutToBeSent"
  int next_ox;
  int next_oy;
  int next_oz;

  bool isPresent() const { return state == Active; }
  bool isCurrent() const { return curr_ox == next_ox && curr_oy == next_oy && curr_oz == next_oz; }

  Octree* octree;
};

#endif // __TERRAINPAGE_H__
